package com.vyooha.asmaak;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class MyDatabaseHelper extends SQLiteOpenHelper {

	private static final String DATABASE_NAME = "DBName";

	private static final int DATABASE_VERSION = 2;

	// Database creation sql statement
	private static final String DATABASE_CREATE_CART = "create table Cart(fishid text,fishname text,isclean text,processings text,quantity integer,price real,cleaningcharge real,imgurl text,uom text);";
	
	public MyDatabaseHelper(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);

	}

	// Method is called during creation of the database
	@Override
	public void onCreate(SQLiteDatabase database) {

		// Log.d("database", "db created");
		database.execSQL(DATABASE_CREATE_CART);
		
	}

	// Method is called during an upgrade of the database,
	@Override
	public void onUpgrade(SQLiteDatabase database, int oldVersion,
			int newVersion) {
		Log.d(MyDatabaseHelper.class.getName(),
				"Upgrading database from version " + oldVersion + " to "
						+ newVersion + ", which will destroy all old data");
		database.execSQL("DROP TABLE IF EXISTS UserTrips");
		
		onCreate(database);
	}
}