package com.vyooha.asmaak;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;  

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.media.AudioManager;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.MeasureSpec;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;
import android.widget.SeekBar.OnSeekBarChangeListener;

import com.google.android.gms.internal.ed;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.OnCameraChangeListener;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.parse.FunctionCallback;
import com.parse.ParseCloud;
import com.parse.ParseException;
import com.parse.ParseUser;


public class Confirm_Activity extends FragmentActivity implements OnCameraChangeListener{
	 LocationManager locationManager;
	//TextView txt_total;
	Button btn_getcurrentlocation,btn_placeorder;
	ImageButton imgback;
	//EditText edit_email,edit_phone,edit_buildno,edit_zone,edit_street,edit_addr,edit_notes;
	ToggleButton tgl_saveaddr;
	LatLng latlng,currentlatlng;
	//ArrayList<String> listofpid;

	SupportMapFragment map;
	
	float totalforpayment=0;
	MyDB db;
	ParseUser user;
	AsmaakApplication appState;
	
	public  ProgressDialog proDialog;
	public  void startLoading() {
	    proDialog = new ProgressDialog(this);
	    proDialog.setMessage("Please Wait.....");
	    proDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
	    proDialog.setCancelable(false);
	    proDialog.show();
	}

    public  void stopLoading() {
	    proDialog.dismiss();
	    proDialog = null;
	}


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		
		super.onCreate(savedInstanceState);
		
		requestWindowFeature(Window.FEATURE_NO_TITLE);
        
//		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
        
//      WindowManager.LayoutParams.FLAG_FULLSCREEN);
		
		setContentView(R.layout.activity_confirm);
		user=ParseUser.getCurrentUser();
		db=new MyDB(getApplicationContext());
		appState=(AsmaakApplication)getApplicationContext();
	
		
		btn_getcurrentlocation=(Button)findViewById(R.id.btn_confirm_getclocation);
		btn_placeorder=(Button)findViewById(R.id.btn_confirm_placeorder);
			
		tgl_saveaddr=(ToggleButton)findViewById(R.id.toggle_confirm_deliverytype);
		
		tgl_saveaddr.setChecked(false);
		
		imgback=(ImageButton)findViewById(R.id.imgbtn_common_back);
		
		
		imgback.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
			finish();
			overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
			}
		});
		
		 totalforpayment=getIntent().getFloatExtra("totalprice", 0);
			
	 appState = ((AsmaakApplication)getApplicationContext());
    	
		
		 locationManager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);

				// Define a listener that responds to location updates
				final LocationListener locationListener = new LocationListener() {
					@Override
				    public void onLocationChanged(Location location) {
				      // Called when a new location is found by the network location provider.
			currentlatlng=new LatLng(location.getLatitude(), location.getLongitude());
			
			
				    }
					@Override
				    public void onStatusChanged(String provider, int status, Bundle extras) {}
					@Override
				    public void onProviderEnabled(String provider) {}
					@Override
				    public void onProviderDisabled(String provider) {}
				  };

				  if (locationManager.getAllProviders().contains(LocationManager.NETWORK_PROVIDER))
					 locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, locationListener);
				  if (locationManager.getAllProviders().contains(LocationManager.GPS_PROVIDER))
				  locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, locationListener);
				
			
				if(currentlatlng!=null){
					latlng=currentlatlng;
				}
		map = (SupportMapFragment) this.getSupportFragmentManager().findFragmentById(R.id.gmap_confrirm_map);
		map.getMap().setMyLocationEnabled(true); 
		
		map.getMap().getUiSettings().setZoomControlsEnabled(true);
		
		
		map.getMap().setOnMapLongClickListener(new GoogleMap.OnMapLongClickListener() {
			
			@Override
			public void onMapLongClick(LatLng point) {
				// TODO Auto-generated method stub
				 latlng=point;
		            map.getMap().clear();
		           Marker marker= map.getMap().addMarker(new MarkerOptions().position(point));
		           marker.setIcon(BitmapDescriptorFactory.fromResource(R.drawable.mapicon));
		          //  Log.e("new lat lng", latlng.toString());
			}
		}); 
		
		
		
		
		//getcurrent_location();
		
		btn_getcurrentlocation.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				
				
				getcurrent_location();
				
		}
		});
		
		
		
		
		
		btn_placeorder.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				
				if(latlng!=null){
			
				if(tgl_saveaddr.isChecked()){
					Toast.makeText(getApplicationContext(), "Online payment feature is not available now", Toast.LENGTH_LONG).show();
				}
				else{
					
					
					if(user!=null){
						orderforUser();
					}
					else{
						orderforguest();
					}
					
					
					
					
				}
				
				
				
				}
				else{
					Toast.makeText(getApplicationContext(), "cant got your location,check your gps or network settings", Toast.LENGTH_LONG).show();
				}
				
			}
		});
		
		
		
		
		
		
	}
	
	public void getcurrent_location(){
		
		boolean gps_enabled = false;
		boolean network_enabled = false;

		try {
		    gps_enabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
		} catch(Exception ex) {}

		try {
		    network_enabled = locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
		} catch(Exception ex) {}

		if(!gps_enabled && !network_enabled) {
		    // notify user
		    AlertDialog.Builder dialog = new AlertDialog.Builder(Confirm_Activity.this);
		    dialog.setMessage("Check your Location Settings first");
		    dialog.setPositiveButton("ok", new DialogInterface.OnClickListener() {
		            @Override
		            public void onClick(DialogInterface paramDialogInterface, int paramInt) {
		                // TODO Auto-generated method stub
		                Intent myIntent = new Intent( Settings.ACTION_LOCATION_SOURCE_SETTINGS);
		               Confirm_Activity.this.startActivity(myIntent);
		                //get gps
		            }
		        });
		    dialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {

		            @Override
		            public void onClick(DialogInterface paramDialogInterface, int paramInt) {
		                // TODO Auto-generated method stub

		            }
		        });
		    dialog.show();      
		}
		
		if(currentlatlng!=null){
			latlng=currentlatlng;
			map.getMap().clear();
			CameraUpdate cameraPosition = CameraUpdateFactory.newLatLngZoom(latlng,16);
		
			map.getMap().animateCamera(cameraPosition);
		
		Marker marker=	map.getMap().addMarker(new MarkerOptions().position(latlng));
		marker.setIcon(BitmapDescriptorFactory.fromResource(R.drawable.mapicon));
		//  Log.e("new lat lng", latlng.toString());
			
		}
		else if(latlng!=null){
			map.getMap().clear();
			CameraUpdate cameraPosition = CameraUpdateFactory.newLatLngZoom(latlng,16);
			map.getMap().animateCamera(cameraPosition);
			Marker marker=map.getMap().addMarker(new MarkerOptions().position(latlng));
			marker.setIcon(BitmapDescriptorFactory.fromResource(R.drawable.mapicon));
		}
		

		
	}
	
public void orderforguest(){
		
	
		
		String jsonstr=appState.pref.getString("orderhead", "0");
		JSONObject json1=new JSONObject();
		if(jsonstr!=null)
			try {
				json1=new JSONObject(jsonstr);
			} catch (JSONException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		
//		json.put("address", txt_address.getText().toString());
//		json.put("phone", txt_phone.getText().toString());
//		json.put("deliveryslot", txt_arrivetime.getText().toString());
//		json.put("comments", txt_comment.getText().toString());
//		
		 String phone="",address="",deliveryslot="",comment="";
		try{
		 phone=json1.getString("phone");
		 address=json1.getString("address");
		 deliveryslot=json1.getString("deliveryslot");
		 comment=json1.getString("comments");
		}
		catch(Exception e){
			
		}
			HashMap<String, Object> inputparm = new HashMap<String, Object>();
			JSONObject jsonhead=new JSONObject();
			try{
			jsonhead.put("phone", phone);
			jsonhead.put("address", address);
			jsonhead.put("delivery_slot", deliveryslot);
			jsonhead.put("comment", comment);
			}
			catch(Exception e){
				
			}
			
				try{
				jsonhead.put("guest_email",getIntent().getStringExtra("email"));
				
				}
				catch(Exception e){
					
				}
				JSONArray jsonarraydetails=new JSONArray();
				
				Cursor c=db.selectCart();
				for(int i=0;i<c.getCount();i++){
					c.moveToPosition(i);
					JSONObject json=new JSONObject();
					try{
					json.put("fish_id", c.getString(0));
					json.put("isCleanRequired", c.getString(2));
					json.put("quantity", c.getInt(4));
					json.put("processings", c.getString(3));
					}
					catch(Exception e){
						
					}
					
					jsonarraydetails.put(json);
				}
				
				
				startLoading();
				inputparm.put("lat", latlng.latitude);
				inputparm.put("lng", latlng.longitude);
				inputparm.put("order_head", jsonhead);
				inputparm.put("order_items", jsonarraydetails);
				ParseCloud.callFunctionInBackground("placeOrderAsGuest", inputparm,
						new FunctionCallback<String>() {

				@Override
				public void done(String result,
						ParseException e) {
					// TODO Auto-generated method stub
					if (e == null) {
						if(proDialog!=null)
							stopLoading();
						finish();
						Intent i=new Intent(getApplicationContext(), ShopActivity.class);
						startActivity(i);
						appState.editor.remove("orderhead");
						appState.editor.commit();
						db.deleteall_Cart();
						Toast.makeText(getApplicationContext(), result, Toast.LENGTH_LONG).show();
					}
					else{

						if(proDialog!=null)
							stopLoading();
						Toast.makeText(getApplicationContext(), "can't make order now", Toast.LENGTH_LONG).show();
					}
					
				}});
				
	}
	
	
public void orderforUser(){
		
		String jsonstr=appState.pref.getString("orderhead", "0");
		JSONObject json=new JSONObject();
		if(jsonstr!=null)
			try {
				json=new JSONObject(jsonstr);
			} catch (JSONException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		
//		json.put("address", txt_address.getText().toString());
//		json.put("phone", txt_phone.getText().toString());
//		json.put("deliveryslot", txt_arrivetime.getText().toString());
//		json.put("comments", txt_comment.getText().toString());
//		
		 String phone="",address="",deliveryslot="",comment="";
		try{
		 phone=json.getString("phone");
		 address=json.getString("address");
		 deliveryslot=json.getString("deliveryslot");
		 comment=json.getString("comments");
		}
		catch(Exception e){
			
		}
			HashMap<String, Object> inputparm = new HashMap<String, Object>();
			
				
			inputparm.put("lat", latlng.latitude);
			inputparm.put("lng", latlng.longitude);
				inputparm.put("phone", phone);
				inputparm.put("delivery_slot", deliveryslot);
				inputparm.put("address", address);
				inputparm.put("comment", comment);
			startLoading();
				ParseCloud.callFunctionInBackground("placeOrderForUser", inputparm,
						new FunctionCallback<String>() {

				@Override
				public void done(String result,
						ParseException e) {
					// TODO Auto-generated method stub
					if (e == null) {
						

						if(proDialog!=null)
							stopLoading();
						
						finish();
						Intent i=new Intent(getApplicationContext(), ShopActivity.class);
						startActivity(i);

						appState.editor.remove("orderhead");
						appState.editor.commit();
						Toast.makeText(getApplicationContext(), result, Toast.LENGTH_LONG).show();
					}
					else{

						if(proDialog!=null)
							stopLoading();
						Toast.makeText(getApplicationContext(), "can't make order now", Toast.LENGTH_LONG).show();
						
					}
					
				}});
			

}
		
		
				
	
	
	
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		//getMenuInflater().inflate(R.menu.near_by_map, menu);
		return true;
	}

	@Override
	public void onCameraChange(CameraPosition arg0) {
		// TODO Auto-generated method stub
		
	}
	
	@Override
	protected void onResume() {

		super.onResume();
		getcurrent_location();
	
	}
	
	 @Override
	  public void onDestroy() {
	    super.onDestroy();
//	   if(widIntent!=null){
//		   stopService(widIntent);
//	   }
	  }


	

	 
}


