package com.vyooha.asmaak;

import android.app.Application;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

import com.nostra13.universalimageloader.cache.memory.impl.WeakMemoryCache;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer;
import com.parse.ConfigCallback;
import com.parse.Parse;
import com.parse.ParseConfig;
import com.parse.ParseException;
import com.parse.ParseFile;



public class AsmaakApplication extends Application {
	public static boolean mMapIsTouched = true;

	public SharedPreferences pref;
	public Editor editor;

	
	public ParseFile imageforslider_1,imageforslider_2,imageforslider_3,imageforslider_4,imageforslider_5;
	String textheadforslider_1,textheadforslider_2,textheadforslider_3,textheadforslider_4,textheadforslider_5;
	String textbodyforslider_1,textbodyforslider_2,textbodyforslider_3,textbodyforslider_4,textbodyforslider_5;
	
	
	@Override
	public void onCreate() {
		super.onCreate();

		
		pref = getApplicationContext().getSharedPreferences("MyPref",
				MODE_PRIVATE);
		editor = pref.edit();

		// Add your initialization code here
		Parse.enableLocalDatastore(this);
		Parse.initialize(this, "OST4drGFEsBLwn55z9enMcDZC1QsLlEb1V60GjBD",
				"imPNE4qrIVAdnynvqiFjGcQ75mbZmiVhq6pUaSc0");
		
		DisplayImageOptions defaultOptions = new DisplayImageOptions.Builder()
				.cacheOnDisc(true).cacheInMemory(true)
				.imageScaleType(ImageScaleType.EXACTLY)
				.displayer(new FadeInBitmapDisplayer(300)).build();

		ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(
				getApplicationContext())
				.defaultDisplayImageOptions(defaultOptions)
				.memoryCache(new WeakMemoryCache())
				.discCacheSize(100 * 1024 * 1024).build();

		ImageLoader.getInstance().init(config);
		
		
		ParseConfig.getInBackground(new ConfigCallback() {
			
			@Override
			public void done(ParseConfig config, ParseException e) {
			
				if(e==null){
				}
				else
				{
					config=ParseConfig.getCurrentConfig();
				}
				
				imageforslider_1=config.getParseFile("imageforslider_1");
				imageforslider_2=config.getParseFile("imageforslider_2");
				imageforslider_3=config.getParseFile("imageforslider_3");
				imageforslider_4=config.getParseFile("imageforslider_4");
				imageforslider_5=config.getParseFile("imageforslider_5");
				
				editor.putString("imageforslider_1", imageforslider_1.getUrl());
				editor.putString("imageforslider_2", imageforslider_2.getUrl());
				editor.putString("imageforslider_3", imageforslider_3.getUrl());
				editor.putString("imageforslider_4", imageforslider_4.getUrl());
				editor.putString("imageforslider_5", imageforslider_5.getUrl());
				
				textbodyforslider_1=config.getString("textbodyforslider_1");
				textbodyforslider_2=config.getString("textbodyforslider_2");
				textbodyforslider_3=config.getString("textbodyforslider_3");
				textbodyforslider_4=config.getString("textbodyforslider_4");
				textbodyforslider_5=config.getString("textbodyforslider_5");
				
				editor.putString("textbodyforslider_1", textbodyforslider_1);
				editor.putString("textbodyforslider_2", textbodyforslider_2);
				editor.putString("textbodyforslider_3", textbodyforslider_3);
				editor.putString("textbodyforslider_4", textbodyforslider_4);
				editor.putString("textbodyforslider_5", textbodyforslider_5);
				
				textheadforslider_1=config.getString("textheadforslider_1");
				textheadforslider_2=config.getString("textheadforslider_2");
				textheadforslider_3=config.getString("textheadforslider_3");
				textheadforslider_4=config.getString("textheadforslider_4");
				textheadforslider_5=config.getString("textheadforslider_5");
				
				editor.putString("textheadforslider_1", textheadforslider_1);
				editor.putString("textheadforslider_2", textheadforslider_2);
				editor.putString("textheadforslider_3", textheadforslider_3);
				editor.putString("textheadforslider_4", textheadforslider_4);
				editor.putString("textheadforslider_5", textheadforslider_5);
				
				editor.commit();
				
			}
		});
		
		
		
		
		
		

	
	}

}