package com.vyooha.asmaak;



import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.view.KeyEvent;
import android.view.Window;
import android.view.WindowManager;


public class Splash_Activity extends Activity {
	
	
	
	private boolean isBackPressed = false;
	private static int SPLASH_TIME_OUT=1500;
  
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		
		 requestWindowFeature(Window.FEATURE_NO_TITLE);
	 	 getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_splash);
		
		 
				new Handler().postDelayed(new Runnable(){
					public void run()
					{   if(!isBackPressed)
					{
						Intent i=new Intent(getApplicationContext(),ShopActivity.class);
						startActivity(i);
						finish();
					}}
				},SPLASH_TIME_OUT);
			
	}
       
    
	
	 @Override
	    public boolean onKeyDown(int keyCode, KeyEvent event) {
	        if ((keyCode == KeyEvent.KEYCODE_BACK)) {
	            isBackPressed = true;
	            finish();
	        }
	        return super.onKeyDown(keyCode, event);

	    }
    @Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
			}
	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		
	}
	
	
	
	}

