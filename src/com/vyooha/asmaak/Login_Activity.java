package com.vyooha.asmaak;


import java.io.File;

import com.parse.LogInCallback;
import com.parse.ParseException;
import com.parse.ParseUser;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences.Editor;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;

public class Login_Activity  extends Activity {
	//declaration of variables
	//TextView forgotpass_txtview,createnew_txtview;
	EditText email_edit,password_edit;
	Button signin_btn;
	Intent intent;
	String email,pass;
	VideoView video1;
	
	private boolean bVideoIsBeingTouched = false;
	private Handler mHandler = new Handler();
	private int media_position = 1;
	public  ProgressDialog proDialog;
	public  void startLoading() {
	    proDialog = new ProgressDialog(this);
	    proDialog.setMessage("Signing in. Please Wait.....");
	    proDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
	    proDialog.setCancelable(false);
	    proDialog.show();
	}

public  void stopLoading() {
	    proDialog.dismiss();
	    proDialog = null;
	}
	
	
	 boolean validationError;
	 String errorMessage;
	
	 @Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		
		setContentView(R.layout.activity_login);
		
		email_edit=(EditText)findViewById(R.id.edit_signin_email);
		
		password_edit=(EditText)findViewById(R.id.edit_signin_password);
		signin_btn=(Button)findViewById(R.id.btn_signin_signin);
//		createnew_txtview=(TextView)findViewById(R.id.txt_signin_signup);
//		forgotpass_txtview=(TextView)findViewById(R.id.txt_signin_forgot);
//	
		password_edit.setTypeface(Typeface.DEFAULT);
		password_edit.setTransformationMethod(new PasswordTransformationMethod());
		
		signin_btn.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				
				email=email_edit.getText().toString();
				pass=password_edit.getText().toString();
				if(email.length()!=0&&pass.length()!=0){
					if(Utilities_Functions.chekEmailId(email)){
					
				if(Utilities_Functions.checkinternet(getApplicationContext())){
					startLoading();
			ParseUser.logInInBackground(email, pass, new LogInCallback() {
				
				@Override
				public void done(ParseUser user, ParseException e) {
					// TODO Auto-generated method stub
					if(e==null){
						
						
					}
				}
			});
				}
				else{
					Toast.makeText(getApplicationContext(), "Please check your internet connection", Toast.LENGTH_LONG).show();
				}
				
				
					}
					else {
						Toast.makeText(getApplicationContext(), "Enter a valid email id", Toast.LENGTH_LONG).show();
						
					}
		}
			else{
				Toast.makeText(getApplicationContext(), "Please add all fields", Toast.LENGTH_LONG).show();
			}
			}
		});
		
		
//		
		
		//create new account
//		createnew_txtview.setOnClickListener(new OnClickListener() {
//			
//			@Override
//			public void onClick(View v) {
//
////				intent=new Intent(getApplicationContext(),CreateNew_Activity.class);
////				startActivity(intent);
//			}
//		});
//		
	 }
		
	
	
	 @Override
		public void onResume(){
		    super.onResume();
		  
		}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
}
