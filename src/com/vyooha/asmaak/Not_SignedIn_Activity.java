package com.vyooha.asmaak;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.database.Cursor;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.parse.FunctionCallback;
import com.parse.LogInCallback;
import com.parse.ParseCloud;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseUser;

public class Not_SignedIn_Activity extends Activity {
	
	private ParseUser currentUser;
	// protected ProgressDialog proDialog;
	private ConnectivityManager conmgr;
	Button btn_continueguest,btn_signin,btn_signup;
	EditText edit_guest_email,edit_email,edit_password;
	public  ProgressDialog proDialog;
	public  void startLoading() {
	    proDialog = new ProgressDialog(this);
	    proDialog.setMessage("Please Wait.....");
	    proDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
	    proDialog.setCancelable(false);
	    proDialog.show();
	}

    public  void stopLoading() {
	    proDialog.dismiss();
	    proDialog = null;
	}

	
	MyDB db;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		
		super.onCreate(savedInstanceState);
		db=new MyDB(getApplicationContext());
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);
		
		setContentView(R.layout.notmember);
		
		btn_continueguest=(Button)findViewById(R.id.btn_notmember_continueguest);
		btn_signin=(Button)findViewById(R.id.btn_notmember_signin);
		btn_signup=(Button)findViewById(R.id.btn_notmember_signup);
		
		edit_email=(EditText)findViewById(R.id.edit_notmember_emailsignin);
		edit_guest_email=(EditText)findViewById(R.id.edit_notmember_emailcontinueguest);
		edit_password=(EditText)findViewById(R.id.edit_notmember_passwordsignin);
		
		btn_continueguest.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				final String email=edit_guest_email.getText().toString();
				if(email.length()!=0&&Utilities_Functions.chekEmailId(email)){
					
	
										finish();
										Intent i=new Intent(getApplicationContext(),Confirm_Activity.class);
										i.putExtra("type", "guest");
										i.putExtra("email", email);
										i.putExtra("totalprice", getIntent().getFloatExtra("totalprice", 0));
										
										startActivity(i);
										
									
					
					
				}
			}
		});
		
		btn_signup.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				finish();
			Intent i=new Intent(getApplicationContext(), LoginAndSignup_Activity.class);
			i.putExtra("totalprice", getIntent().getFloatExtra("totalprice", 0));
			i.putExtra("time", "order");
			startActivity(i);	
			}
		});
		
		btn_signin.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				String email,pass;
				email=edit_email.getText().toString();
				pass=edit_password.getText().toString();
				if(email.length()!=0&&pass.length()!=0){
					if(Utilities_Functions.chekEmailId(email)){
					
				if(Utilities_Functions.checkinternet(getApplicationContext())){
				startLoading();
			ParseUser.logInInBackground(email, pass, new LogInCallback() {
				
				@Override
				public void done(ParseUser user, ParseException e) {
					// TODO Auto-generated method stub
					
					
					if(e==null){
//add all items to user cart
						
						JSONArray jsonarray=new JSONArray();
						
						
						Cursor c=db.selectCart();
						
						for(int i=0;i<c.getCount();i++){
							c.moveToPosition(i);
							JSONObject json=new JSONObject();
							// {"fishid","fishname","isclean","processings","quantity","price","cleaningcharge","imgurl","uom"
							try{
							json.put("fish_id", c.getString(0));
							json.put("isCleanRequired", c.getString(2));
							json.put("fish_quan", c.getInt(4));
							ArrayList<String> processings=new ArrayList<String>();
							processings.add(c.getString(3));
							json.put("processings",processings);
							}
							catch (Exception e1){
								
							}
							jsonarray.put(json);
						}
						
						HashMap<String, Object> input_param = new HashMap<String, Object>();
						input_param.put("fish_array", jsonarray);
				
						
						//This function not defined on cloud
						
						
						ParseCloud.callFunctionInBackground("addManyToCart", input_param,
								new FunctionCallback<String>() {

									@Override
									public void done(String result,
											ParseException e) {
										// TODO Auto-generated method stub
										if (e == null) {
											
											if(proDialog!=null)
												stopLoading();
											db.deleteall_Cart();
											finish();
											Intent i=new Intent(getApplicationContext(),Confirm_Activity.class);
											i.putExtra("type", "user");
											
											i.putExtra("totalprice", getIntent().getFloatExtra("totalprice", 0));
											startActivity(i);
										}
										else{
											if(proDialog!=null)
												stopLoading();
											Toast.makeText(getApplicationContext(), "can't sign in now", Toast.LENGTH_LONG).show();
											
											
										}
									}});
						
						
						
					
					}
					else{
						if(proDialog!=null)
							stopLoading();
						Toast.makeText(getApplicationContext(), "can't sign in now", Toast.LENGTH_LONG).show();
						
					}
					
					
					
				}
			});
				}
				else{
					Toast.makeText(getApplicationContext(), "Please check your internet connection", Toast.LENGTH_LONG).show();
				}
				
				
					}
					else {
						Toast.makeText(getApplicationContext(), "Enter a valid email id", Toast.LENGTH_LONG).show();
						
					}
		}
			else{
				Toast.makeText(getApplicationContext(), "Please add all fields", Toast.LENGTH_LONG).show();
			}
				
				
				
			}
		});
		
		
		
	}
}
