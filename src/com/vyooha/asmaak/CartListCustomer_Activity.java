package com.vyooha.asmaak;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.parse.FunctionCallback;
import com.parse.ParseCloud;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseUser;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.provider.ContactsContract.Intents;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

public class CartListCustomer_Activity extends Activity {
	
	ArrayAdapter<CartItemClass> contactAdapter;
	ListView list;
	ParseUser user;
	MyDB db;
	
	static final int TIME_DIALOG_ID = 999;
	private int hour=12;
	private int minute=00;
	
	TextView txt_totalprice;//txt_time,txt_payment;
	ImageButton imgbtn_time,imgbtn_payment,imgbtn_back;
	
	TextView txt_noitems;
	ProgressWheel progress;
	boolean result0=false;
	
	float totalprice=0;
	
	public  ProgressDialog proDialog;
	public  void startLoading() {
	    proDialog = new ProgressDialog(this);
	    proDialog.setMessage("Please Wait.....");
	    proDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
	    proDialog.setCancelable(false);
	    proDialog.show();
	}

    public  void stopLoading() {
	    proDialog.dismiss();
	    proDialog = null;
	}
	
	final DisplayImageOptions options = new DisplayImageOptions.Builder()
	.cacheInMemory(true).cacheOnDisc(true)
	.resetViewBeforeLoading(true)
	// .showImageForEmptyUri(fallback)
	// .showImageOnFail(fallback)
	// .showImageOnLoading(fallback)
	.build();
	
	
//	private TimePickerDialog.OnTimeSetListener timePickerListener = 
//            new TimePickerDialog.OnTimeSetListener() {
//		public void onTimeSet(TimePicker view, int selectedHour,
//				int selectedMinute) {
//			hour = selectedHour;
//			minute = selectedMinute;
// 
//			// set current time into textview
//			txt_time.setText(new StringBuilder().append(pad(hour))
//					.append(":").append(pad(minute)));
//  
//		}
//	};
// 
//	private  String pad(int c) {
//		if (c >= 10)
//		   return String.valueOf(c);
//		else
//		   return "0" + String.valueOf(c);
//	}
//	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState); 
		user=ParseUser.getCurrentUser();
		db=new MyDB(getApplicationContext());
		
		contactAdapter = new ArrayAdapter<CartItemClass>(this, 0) {
			@Override
			public View getView(int position, View convertView, ViewGroup parent) {
				if (convertView == null) {
					convertView = getLayoutInflater().inflate(
							R.layout.cartitem, null);
				}
 
				final CartItemClass it=getItem(position);

				ImageView imageView = (ImageView) convertView
							.findViewById(R.id.img_cartitem_itemimg);
					
					TextView txt_name = (TextView) convertView
							.findViewById(R.id.txt_cartitem_name);
						TextView txt_processingtype=(TextView)convertView.findViewById(R.id.txt_cartitem_cleaning_charge);
						TextView txt_cartitem_subtotal=(TextView)convertView.findViewById(R.id.txt_cartitem_subtotal);
						TextView txt_cartitem_qty=(TextView)convertView.findViewById(R.id.txt_cartitem_quantity);
						final EditText edit_qty=(EditText)convertView.findViewById(R.id.edit_cartitem_quantity);
						ImageLoader imageLoader = ImageLoader.getInstance();
						
						Button btn_add=(Button)convertView.findViewById(R.id.btn_cartitem_add);
						Button btn_sub=(Button)convertView.findViewById(R.id.btn_cartitem_sub);
						
						txt_name.setText(it.name);
						txt_processingtype.setText(it.processings);
						edit_qty.setText(it.quantity+"");
						float subtotal=0;
						if(it.iscleanRequiered){
							subtotal=it.quantity*(it.cleaning_charge+it.unit_price);
						}
						else
							subtotal=it.quantity*it.unit_price;
						
						txt_cartitem_subtotal.setText("QR "+subtotal);
						
						txt_cartitem_qty.setText("/"+it.quantity+" "+it.uom);
					
					
						btn_add.setOnClickListener(new OnClickListener() {
			
							@Override
							public void onClick(View v) {
								it.quantity++;
								edit_qty.setText(Integer.toString(it.quantity));
								contactAdapter.notifyDataSetChanged();
								if(user==null){
									db.updateCartQuantity(it.obid, it.quantity);
								}
								else{
									
									
									it.handler.removeCallbacksAndMessages(null);
									it.handler.postDelayed(new Runnable() {
									    @Override
									    public void run() {
									       Log.e("handler  working  for ",it.name+",qty="+it.quantity);
									    
									       Map<String, Object> inputparm = new HashMap<String, Object>();
											
											inputparm.put("quantity", it.quantity);
											inputparm.put("fish_id", it.fishidsignin);
											
											inputparm.put("isCleanRequired", it.iscleanRequiered);
											ArrayList<String> processings=new ArrayList<String>();
											processings.add(it.processings);
											inputparm.put("processings", processings);
								
												try {
													ParseCloud.callFunction("addToCart", inputparm);
												} catch (ParseException e) {
													// TODO Auto-generated catch block
													e.printStackTrace();
												}	       
									       
									       
									       
									       
									       
									       
									    }
									}, 5000);
								}
								
								
							}
						});
						btn_sub.setOnClickListener(new OnClickListener() {
			
							@Override
							public void onClick(View v) {
								if(it.quantity>1)
									it.quantity--;
								edit_qty.setText(Integer.toString(it.quantity));
								contactAdapter.notifyDataSetChanged();
								
								if(user==null){
									db.updateCartQuantity(it.obid, it.quantity);
								}
								else{
									
									
									it.handler.removeCallbacksAndMessages(null);
									it.handler.postDelayed(new Runnable() {
									    @Override
									    public void run() {
									       Log.e("handler  working  for ",it.name+",qty="+it.quantity);
									    
									       Map<String, Object> inputparm = new HashMap<String, Object>();
											
											inputparm.put("quantity", it.quantity);
											inputparm.put("fish_id", it.fishidsignin);
											
											inputparm.put("isCleanRequired", it.iscleanRequiered);
											ArrayList<String> processings=new ArrayList<String>();
											processings.add(it.processings);
											inputparm.put("processings", processings);
								
												try {
													ParseCloud.callFunction("addToCart", inputparm);
												} catch (ParseException e) {
													// TODO Auto-generated catch block
													e.printStackTrace();
												}	       
									       
									       
									       
									       
									       
									       
									    }
									}, 5000);
								}
								
								
							
							
							}
							});
					
					if (it.imgurl != null) {
						imageLoader.displayImage(it.imgurl, imageView);

					} else {
						imageView.setImageResource(R.drawable.ic_launcher);
					}
					
				
				return convertView;
			}
		};

		
		
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);
		
		setContentView(R.layout.activity_cartlist);
		list = (ListView)findViewById(R.id.listview_cartlist_list);
		list.setAdapter(contactAdapter);
		
//		txt_payment=(TextView)findViewById(R.id.txt_cartlist_paymentmethode);
//		txt_time=(TextView)findViewById(R.id.txt_cartlist_time);
		txt_totalprice=(TextView)findViewById(R.id.txt_cartlist_totalprice);
		txt_noitems=(TextView)findViewById(R.id.txt_cartlist_noitems);
		progress=(ProgressWheel)findViewById(R.id.progress_cartlist_progress);
		
		
		txt_noitems.setVisibility(View.GONE);
		progress.setVisibility(View.VISIBLE);
		progress.spin();
		list.setVisibility(View.GONE);
		
		
		
//		imgbtn_payment=(ImageButton)findViewById(R.id.imgbtn_cartlist_paymentmethode);
//		imgbtn_time=(ImageButton)findViewById(R.id.imgbtn_cartlist_time);
		imgbtn_back=(ImageButton)findViewById(R.id.imgbtn_common_back);
		imgbtn_back.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
			finish();	
			
			overridePendingTransition(R.anim.slide_out_left, R.anim.slide_out_right);
			
			}
		});
		
		
		
		
		txt_totalprice.setText("QR "+totalprice);
		
//		final Calendar c = Calendar.getInstance();
//		hour = c.get(Calendar.HOUR_OF_DAY);
//		minute = c.get(Calendar.MINUTE);
//		txt_time.setText(new StringBuilder().append(pad(hour))
//				.append(":").append(pad(minute)));

		
//		txt_payment.setOnClickListener(new OnClickListener() {
//			
//			@Override
//			public void onClick(View v) {
//			
//				alertlistpayment();
//			}
//		});
//		imgbtn_payment.setOnClickListener(new OnClickListener() {
//			
//			@Override
//			public void onClick(View v) {
//				// TODO Auto-generated method stub
//				alertlistpayment();
//			}
//		});
//		
//		txt_time.setOnClickListener(new OnClickListener() {
//			
//			@Override
//			public void onClick(View v) {
//				// TODO Auto-generated method stub
//				new TimePickerDialog(CartListCustomer_Activity.this, 
//                        timePickerListener, hour, minute,false);
//
//			}
//		});
//		
//		imgbtn_time.setOnClickListener(new OnClickListener() {
//			
//			@Override
//			public void onClick(View v) {
//				// TODO Auto-generated method stub
//				new TimePickerDialog(CartListCustomer_Activity.this, 
//                        timePickerListener, hour, minute,false);
//
//			}
//		});
//		
		if(user==null){
			
			progress.stopSpinning();
			
			progress.setVisibility(View.GONE);
			
			
			
			Cursor c1=db.selectCart();
			if(c1.getCount()>0){
				txt_noitems.setVisibility(View.GONE);
				list.setVisibility(View.VISIBLE);
				result0=true;
			for(int i=0;i<c1.getCount();i++){
				c1.moveToPosition(i);
				CartItemClass cartitclass=new CartItemClass();
				cartitclass.name=c1.getString(1);
				cartitclass.obid=c1.getString(0);
				cartitclass.iscleanRequiered=Boolean.parseBoolean(c1.getString(2));
				cartitclass.processings=c1.getString(3);
				cartitclass.quantity=c1.getInt(4);
				cartitclass.unit_price=c1.getFloat(5);
				cartitclass.cleaning_charge=c1.getFloat(6);
				cartitclass.imgurl=c1.getString(7);
				cartitclass.uom=c1.getString(8);
				cartitclass.fishidsignin=cartitclass.obid;
				
				float subtotal=0;
				if(cartitclass.iscleanRequiered){
					subtotal=cartitclass.cleaning_charge*cartitclass.quantity*cartitclass.unit_price;
				}
				else
					subtotal=cartitclass.quantity*cartitclass.unit_price;
			
				totalprice+=subtotal;
				// {"fishid","fishname","isclean","processings","quantity","price","cleaningcharge","imgurl","uom"
			contactAdapter.add(cartitclass);
			}
			
			}
			
			else{
				txt_noitems.setVisibility(View.VISIBLE);
				list.setVisibility(View.GONE);
				result0=false;
			}
			
			
			
			txt_totalprice.setText("QR "+totalprice);
			
		}
		else{
		HashMap<String, String> input_param = new HashMap<String, String>();
		
		ParseCloud.callFunctionInBackground("viewCart", input_param,
				new FunctionCallback<List<ParseObject>>() {

					@Override
					public void done(List<ParseObject> result,
							ParseException e) {
						// TODO Auto-generated method stub
						progress.stopSpinning();
						
						progress.setVisibility(View.GONE);
						
						
						if (e == null) {
							
							if(result.size()>0){
								list.setVisibility(View.VISIBLE);
								txt_noitems.setVisibility(View.GONE);
								result0=true;
								
							for(int i=0;i<result.size();i++){
								ParseObject item=result.get(i);
								ParseObject fish=item.getParseObject("fish_obj");
								CartItemClass cartitclass=new CartItemClass();
								cartitclass.cleaning_charge=Float.parseFloat(fish.getNumber("processing_charge").toString());
								
								try{
								cartitclass.imgurl=fish.getParseFile("fish_img").getUrl();
								}
								catch(Exception e2){
									
								}
								
								cartitclass.iscleanRequiered=item.getBoolean("isCleanRequired");
								cartitclass.name=fish.getString("fish_name");
								cartitclass.obid=item.getObjectId();
								cartitclass.fishidsignin=fish.getObjectId();
								try {
									cartitclass.processings=item.getJSONArray("processings").getString(0);
								} catch (JSONException e1) {
									// TODO Auto-generated catch block
									e1.printStackTrace();
								}
								cartitclass.quantity=item.getInt("quantity");
								cartitclass.unit_price=(float) fish.getDouble("unit_price");
								cartitclass.uom=fish.getString("uom");
								
								float subtotal=0;
								if(cartitclass.iscleanRequiered){
									subtotal=cartitclass.quantity*(cartitclass.cleaning_charge+cartitclass.unit_price);
								}
								else
									subtotal=cartitclass.quantity*cartitclass.unit_price;
							
								totalprice+=subtotal;
								
								
								contactAdapter.add(cartitclass);
							
							}}
							else {
								
								list.setVisibility(View.GONE);
								
								txt_noitems.setVisibility(View.VISIBLE);
								result0=false;
							}
							
							txt_totalprice.setText("QR "+totalprice);
							
						}
						else {
							list.setVisibility(View.GONE);
							
							txt_noitems.setVisibility(View.VISIBLE);
							txt_noitems.setText("Please check your internet");
							
						}
						
					
					
					}
					
		
		});
		
		}
		Button confirm=(Button)findViewById(R.id.btn_cartlist_confirm);
		confirm.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				
				if(result0){
			
						finish();
						Intent i=new Intent(getApplicationContext(),OrderConfirmation_Activity.class);
				
						i.putExtra("totalprice", totalprice);
				
						startActivity(i);
					}
				else{
					
					Toast.makeText(getApplicationContext(), "No items on cart", Toast.LENGTH_LONG).show();
				}
			}
		});
		
	}
	
	@Override
	public void onBackPressed() {
		finish();
		overridePendingTransition(R.anim.slide_out_left, R.anim.slide_out_right);
	}

//public void alertlistpayment(){
//	
//	AlertDialog.Builder builderSingle = new AlertDialog.Builder(
//            CartListCustomer_Activity.this);
//    builderSingle.setIcon(R.drawable.ic_launcher);
//    builderSingle.setTitle("Select Payment Type:-");
//    final ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(
//            CartListCustomer_Activity.this,
//            android.R.layout.select_dialog_singlechoice);
//    arrayAdapter.add("VISA");
//    arrayAdapter.add("DEBIT");
//    arrayAdapter.add("CREDIT");
//    builderSingle.setNegativeButton("cancel",
//            new DialogInterface.OnClickListener() {
//
//                @Override
//                public void onClick(DialogInterface dialog, int which) {
//                    dialog.dismiss();
//                }
//            });
//
//    builderSingle.setAdapter(arrayAdapter,
//            new DialogInterface.OnClickListener() {
//
//                @Override
//                public void onClick(DialogInterface dialog, int which) {
//                    String strName = arrayAdapter.getItem(which);
//                  txt_payment.setText(strName);
//                }
//            });
//    builderSingle.show();
//
//
//	
//}
}
