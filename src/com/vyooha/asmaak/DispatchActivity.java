package com.vyooha.asmaak;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.Window;
import android.view.WindowManager;
import com.parse.ParseUser;


public class DispatchActivity extends Activity {
	
	private ParseUser currentUser;
	// protected ProgressDialog proDialog;
	private ConnectivityManager conmgr;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		
		super.onCreate(savedInstanceState);

		requestWindowFeature(Window.FEATURE_NO_TITLE);
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);
		
		setContentView(R.layout.splashscreen);
		
		new Handler().postDelayed(new Runnable(){
			public void run()
			{   
				dispatchactivity();
				
				
			}
		},1500);
		// Check if there is current user info
		
	}

	public void dispatchactivity() {
		
		
		currentUser = ParseUser.getCurrentUser();
		
	
		if (currentUser != null) {

			Intent intent = new Intent(this, ItemListCustomer_Activity.class);
			intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK
					| Intent.FLAG_ACTIVITY_NEW_TASK);
			startActivity(intent);
		} else {
			Log.e("rendering", "not logged in so to welcome page");
			// Start and intent for the logged out activity
			Intent intent = new Intent(this, LoginAndSignup_Activity.class);
			intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK
					| Intent.FLAG_ACTIVITY_NEW_TASK);
			startActivity(intent);

		}
		
	}

}
