package com.vyooha.asmaak;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.json.JSONException;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Point;
import android.os.Bundle;
import android.util.Log;
import android.view.Display;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.ViewFlipper;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.parse.FunctionCallback;
import com.parse.ParseCloud;
import com.parse.ParseException;
import com.parse.ParseObject;

public class ItemListCustomer_Activity extends Activity {
	
	ArrayAdapter<ItemClass> contactAdapter;
	GridView list;
	
	ImageButton imgbtn_cart,imgbtn_back;
	//EditText edit_search;
	TextView txt_head,txt_noitem;
	ProgressWheel progressdialogue;
	
	AsmaakApplication appState;
	//Declaration for slider
	ViewFlipper slider;
	ImageView img_imageforslider_1,img_imageforslider_2,img_imageforslider_3,img_imageforslider_4,img_imageforslider_5;
	TextView txt_textheadforslider_1,txt_textheadforslider_2,txt_textheadforslider_3,txt_textheadforslider_4,txt_textheadforslider_5;
	TextView txt_textbodyforslider_1,txt_textbodyforslider_2,txt_textbodyforslider_3,txt_textbodyforslider_4,txt_textbodyforslider_5;
	
	
	final DisplayImageOptions options = new DisplayImageOptions.Builder()
	.cacheInMemory(true).cacheOnDisc(true)
	.resetViewBeforeLoading(true)
	// .showImageForEmptyUri(fallback)
	// .showImageOnFail(fallback)
	// .showImageOnLoading(fallback)
	.build();
	@SuppressLint("NewApi")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState); 
		appState = ((AsmaakApplication) getApplicationContext());
		contactAdapter = new ArrayAdapter<ItemClass>(this, 0) {
			@Override
			public View getView(int position, View convertView, ViewGroup parent) {
				if (convertView == null) {
					convertView = getLayoutInflater().inflate(
							R.layout.items_item, null);
				}
 
				final ItemClass it=getItem(position);

				ImageView imageView = (ImageView) convertView
							.findViewById(R.id.img_item_itemimg);
					
					TextView txt_name = (TextView) convertView
							.findViewById(R.id.txt_item_name);
					TextView txt_bestfor=(TextView)convertView.findViewById(R.id.txt_item_bestfor);
				//	TextView txt_fresness=(TextView)convertView.findViewById(R.id.txt_item_freshness);
					TextView txt_price=(TextView)convertView.findViewById(R.id.txt_item_price);
				//	TextView txt_cleaning_charge=(TextView)convertView.findViewById(R.id.txt_item_cleaning_charge);
					ImageLoader imageLoader = ImageLoader.getInstance();
					LinearLayout ll=(LinearLayout)convertView.findViewById(R.id.ll_item_layoutall);
				//	Button btn_ordernow=(Button)convertView.findViewById(R.id.btn_item_ordernow);
					txt_name.setText(it.name);
					txt_price.setText("QR "+it.price+"/KG");
					txt_bestfor.setText(it.bestfor);
					
					//txt_cleaning_charge.setText(it.cleaning_charge);
				//	txt_fresness.setText(it.freshnes_scale);

					if (it.img != null) {
						imageLoader.displayImage(it.img, imageView);

					} else {
						imageView.setImageResource(R.drawable.ic_launcher);
					}
					
					ll.setOnClickListener(new  OnClickListener() {
						
						@Override
						public void onClick(View v) {
							
							Intent i=new Intent(getApplicationContext(),AddToCart.class);
							i.putExtra("name", it.name);
							i.putExtra("processingcharge", it.processing_charge);
							i.putExtra("freshness_scale", it.freshnes_scale);
							i.putExtra("price", it.price);
							i.putExtra("itemid", it.obid);
							i.putExtra("imageurl",it.img );
							i.putExtra("uom", it.uom_type);
							i.putExtra("fish_desc", it.description);
							i.putExtra("processings", it.processings);
							startActivity(i);
							
						}
					});

				return convertView;
			}
		};

		requestWindowFeature(Window.FEATURE_NO_TITLE);
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);
		
		
		
		setContentView(R.layout.activity_itemlist_customer);
		list = (GridView)findViewById(R.id.gridview_item_customer);
		list.setAdapter(contactAdapter);
	//	edit_search=(EditText)findViewById(R.id.edit_item_search);
		imgbtn_cart=(ImageButton)findViewById(R.id.imgbtn_item_search);
		txt_head=(TextView)findViewById(R.id.txt_item_head);
		imgbtn_back=(ImageButton)findViewById(R.id.imgbtn_common_back);
		//edit_search.setVisibility(View.GONE);
		
		txt_noitem=(TextView)findViewById(R.id.txt_item_noitems);
		progressdialogue=(ProgressWheel)findViewById(R.id.progress_item_progress);
		
		slider=(ViewFlipper)findViewById(R.id.viewflipper_item_flipperitem);
		
		ImageLoader imageLoader = ImageLoader.getInstance();
		//initialization for slider
		img_imageforslider_1=(ImageView)findViewById(R.id.img_item_imageforslider_1);
		img_imageforslider_2=(ImageView)findViewById(R.id.img_item_imageforslider_2);
		img_imageforslider_3=(ImageView)findViewById(R.id.img_item_imageforslider_3);
		img_imageforslider_4=(ImageView)findViewById(R.id.img_item_imageforslider_4);
		img_imageforslider_5=(ImageView)findViewById(R.id.img_item_imageforslider_5);
		
		txt_textheadforslider_1=(TextView)findViewById(R.id.txt_item_textheadforslider_1);
		txt_textheadforslider_2=(TextView)findViewById(R.id.txt_item_textheadforslider_2);
		txt_textheadforslider_3=(TextView)findViewById(R.id.txt_item_textheadforslider_3);
		txt_textheadforslider_4=(TextView)findViewById(R.id.txt_item_textheadforslider_4);
		txt_textheadforslider_5=(TextView)findViewById(R.id.txt_item_textheadforslider_5);
		
		txt_textbodyforslider_1=(TextView)findViewById(R.id.txt_item_textbodyforslider_1);
		txt_textbodyforslider_2=(TextView)findViewById(R.id.txt_item_textbodyforslider_2);
		txt_textbodyforslider_3=(TextView)findViewById(R.id.txt_item_textbodyforslider_3);
		txt_textbodyforslider_4=(TextView)findViewById(R.id.txt_item_textbodyforslider_4);
		txt_textbodyforslider_5=(TextView)findViewById(R.id.txt_item_textbodyforslider_5);
		
		
		//set text body of all slider item
//		Display display = getWindowManager().getDefaultDisplay();
//		Point size = new Point();
//		display.getSize(size);
//		int width = size.x;
//		int height = size.y;
//		Log.e("screen width: ", width+"");
//		
		if(appState.textbodyforslider_1!=null)
			txt_textbodyforslider_1.setText(appState.textbodyforslider_1);
		else{
			if(appState.pref.contains("textbodyforslider_1"))
				txt_textbodyforslider_1.setText(appState.pref.getString("textbodyforslider_1", ""));
			else
				txt_textbodyforslider_1.setVisibility(View.GONE);
			
			}
		if(appState.textbodyforslider_2!=null)
			txt_textbodyforslider_2.setText(appState.textbodyforslider_2);
		else{
			if(appState.pref.contains("textbodyforslider_2"))
				txt_textbodyforslider_2.setText(appState.pref.getString("textbodyforslider_2", ""));
			else
				txt_textbodyforslider_2.setVisibility(View.GONE);
			
			}
		
		if(appState.textbodyforslider_3!=null)
			txt_textbodyforslider_3.setText(appState.textbodyforslider_3);
		else{
			if(appState.pref.contains("textbodyforslider_3"))
				txt_textbodyforslider_3.setText(appState.pref.getString("textbodyforslider_3", ""));
			else
				txt_textbodyforslider_3.setVisibility(View.GONE);
			
			}
		
		if(appState.textbodyforslider_4!=null)
			txt_textbodyforslider_4.setText(appState.textbodyforslider_4);
		else{
			if(appState.pref.contains("textbodyforslider_4"))
				txt_textbodyforslider_4.setText(appState.pref.getString("textbodyforslider_4", ""));
			else
				txt_textbodyforslider_4.setVisibility(View.GONE);
			
			}
		if(appState.textbodyforslider_5!=null)
			txt_textbodyforslider_5.setText(appState.textbodyforslider_5);
		else{
			if(appState.pref.contains("textbodyforslider_5"))
				txt_textbodyforslider_5.setText(appState.pref.getString("textbodyforslider_5", ""));
			else
				txt_textbodyforslider_5.setVisibility(View.GONE);
			
			}
		//set text head for each slider item
		
		if(appState.textheadforslider_1!=null)
			txt_textheadforslider_1.setText(appState.textheadforslider_1);
		else{
			if(appState.pref.contains("textheadforslider_1"))
				txt_textheadforslider_1.setText(appState.pref.getString("textheadforslider_1", ""));
			else
				txt_textheadforslider_1.setVisibility(View.GONE);
			
			}
		if(appState.textheadforslider_2!=null)
			txt_textheadforslider_2.setText(appState.textheadforslider_2);
		else{
			if(appState.pref.contains("textheadforslider_2"))
				txt_textheadforslider_2.setText(appState.pref.getString("textheadforslider_2", ""));
			else
				txt_textheadforslider_2.setVisibility(View.GONE);
			
			}
		
		if(appState.textheadforslider_3!=null)
			txt_textheadforslider_3.setText(appState.textheadforslider_3);
		else{
			if(appState.pref.contains("textheadforslider_3"))
				txt_textheadforslider_3.setText(appState.pref.getString("textheadforslider_3", ""));
			else
				txt_textheadforslider_3.setVisibility(View.GONE);
			
			}
		
		if(appState.textheadforslider_4!=null)
			txt_textheadforslider_4.setText(appState.textheadforslider_4);
		else{
			if(appState.pref.contains("textheadforslider_4"))
				txt_textheadforslider_4.setText(appState.pref.getString("textheadforslider_4", ""));
			else
				txt_textheadforslider_4.setVisibility(View.GONE);
			
			}
		
		if(appState.textheadforslider_5!=null)
			txt_textheadforslider_5.setText(appState.textheadforslider_5);
		else{
			if(appState.pref.contains("textheadforslider_5"))
				txt_textheadforslider_5.setText(appState.pref.getString("textheadforslider_5", ""));
			else
				txt_textheadforslider_5.setVisibility(View.GONE);
			
			}
		
		
		
		//set images for each slider item
		
		if(appState.imageforslider_1!=null)
		imageLoader.displayImage(appState.imageforslider_1.getUrl(), img_imageforslider_1);
		else{
			if(appState.pref.contains("imageforslider_1"))
			imageLoader.displayImage(appState.pref.getString("imageforslider_1", ""),img_imageforslider_1);
			else
				img_imageforslider_1.setImageResource(R.drawable.ic_launcher);
			}
		
		
		if(appState.imageforslider_2!=null)
			imageLoader.displayImage(appState.imageforslider_2.getUrl(), img_imageforslider_2);
		else{
			if(appState.pref.contains("imageforslider_2"))
			imageLoader.displayImage(appState.pref.getString("imageforslider_2", ""),img_imageforslider_2);
			else
				img_imageforslider_2.setImageResource(R.drawable.ic_launcher);
			}
		
		if(appState.imageforslider_3!=null)
			imageLoader.displayImage(appState.imageforslider_3.getUrl(), img_imageforslider_3);
		else{
			if(appState.pref.contains("imageforslider_3"))
			imageLoader.displayImage(appState.pref.getString("imageforslider_3", ""),img_imageforslider_3);
			else
				img_imageforslider_3.setImageResource(R.drawable.ic_launcher);
			}
		if(appState.imageforslider_4!=null)
			imageLoader.displayImage(appState.imageforslider_4.getUrl(), img_imageforslider_4);
		else{
			if(appState.pref.contains("imageforslider_4"))
			imageLoader.displayImage(appState.pref.getString("imageforslider_4", ""),img_imageforslider_4);
			else
				img_imageforslider_4.setImageResource(R.drawable.ic_launcher);
			}
		if(appState.imageforslider_5!=null)
			imageLoader.displayImage(appState.imageforslider_5.getUrl(), img_imageforslider_5);
		else{
			if(appState.pref.contains("imageforslider_5"))
			imageLoader.displayImage(appState.pref.getString("imageforslider_5", ""),img_imageforslider_5);
			else
				img_imageforslider_5.setImageResource(R.drawable.ic_launcher);
			}
		
		slider.setFlipInterval(3000);
		slider.setInAnimation(this,R.anim.slide_in_right);
		slider.setOutAnimation(this, R.anim.slide_out_left);
		slider.startFlipping();
		
		slider.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
			
				if(slider.isFlipping()){
					slider.stopFlipping();
					
				}
				else
					slider.startFlipping();
			}
		});
		
		
		
		progressdialogue.setVisibility(View.VISIBLE);
		progressdialogue.spin();
		
		list.setVisibility(View.GONE);
		txt_noitem.setVisibility(View.GONE);
	
		
		
		
		
		HashMap<String, String> input_param = new HashMap<String, String>();
		String category;
		category=getIntent().getStringExtra("category");
		
		imgbtn_back.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
			
				finish();
				overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
			}
		});
		
		if(category.equals("all"))
		input_param.put("tag_filter","" );
		else 
			input_param.put("tag_filter", category);
		ParseCloud.callFunctionInBackground("getAvailableFishesByTag", input_param,
				new FunctionCallback<List<ParseObject>>() {

					@Override
					public void done(List<ParseObject> result,
							ParseException e) {
						progressdialogue.setVisibility(View.GONE);
						progressdialogue.stopSpinning();
					
						// TODO Auto-generated method stub
						if (e == null) {
							
							if(result.size()>0){
								
									list.setVisibility(View.VISIBLE);
									txt_noitem.setVisibility(View.GONE);
								
							for(int i=0;i<result.size();i++){
								ParseObject item=result.get(i);
								ItemClass itclass=new ItemClass();
								itclass.name=item.getString("fish_name");
								try{
								itclass.img=item.getParseFile("fish_img").getUrl();
								}
								catch(Exception e1){
									
								}
								itclass.obid=item.getObjectId();
								itclass.description=item.getString("fish_desc");
								itclass.freshnes_scale=item.getString("freshness_scale");
								itclass.processing_charge=item.getNumber("processing_charge").toString();
								itclass.bestfor=item.getString("best_for");
								
								itclass.price=item.getNumber("unit_price").toString();
								itclass.uom_type=item.getString("uom");
								
								ArrayList<String> processings=new  ArrayList<String>();
								for(int j=0;j<item.getJSONArray("processings").length();j++)
								{
									try {
										processings.add(item.getJSONArray("processings").getString(j));
									} catch (JSONException e1) {
										// TODO Auto-generated catch block
										e1.printStackTrace();
									}
								}
								itclass.processings=processings;
								contactAdapter.add(itclass);
								Log.e("retrieved object", item.toString());
							}
							
						}
							else {
								list.setVisibility(View.GONE);
							
								txt_noitem.setVisibility(View.VISIBLE);
								}
						
					
					
					}
						else {
							txt_noitem.setVisibility(View.VISIBLE);
							txt_noitem.setText("Please check your internet connection");
							list.setVisibility(View.GONE);
						}
						
					}
					
		});
		
		
		imgbtn_cart.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Intent intent=new Intent(getApplicationContext(), CartListCustomer_Activity.class);
				startActivity(intent);
				overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
//				edit_search.setVisibility(View.VISIBLE);
//				txt_head.setVisibility(View.GONE);
			}
		});
		
//		edit_search.setOnEditorActionListener(new TextView.OnEditorActionListener() {
//		    @Override
//		    public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
//		        if (actionId == EditorInfo.IME_ACTION_SEARCH) {
//		        	
//		        	String tag=edit_search.getText().toString();
//		            searchText();
//		        	
//		            edit_search.setVisibility(View.GONE);
//		        	txt_head.setVisibility(View.VISIBLE);
//		        	
//		        	InputMethodManager imm = (InputMethodManager) getSystemService(
//		    				Context.INPUT_METHOD_SERVICE);
//		    		imm.hideSoftInputFromWindow(edit_search.getWindowToken(), 0);
//		    		
//		            return true;		            
//		        }
//		        return false;
//		    }
//		}); 
		
		
		
		
	}

//	public void searchText(){
//		String tag=edit_search.getText().toString();
//		contactAdapter.clear();
//		contactAdapter.notifyDataSetChanged();
//		HashMap<String, String> input_param = new HashMap<String, String>();
//		Log.e("string passed", tag);
//		input_param.put("tag_filter", tag);
//		ParseCloud.callFunctionInBackground("getAvailableFishesByTag", input_param,
//				new FunctionCallback<List<ParseObject>>() {
//
//					@Override
//					public void done(List<ParseObject> result,
//							ParseException e) {
//						// TODO Auto-generated method stub
//						if (e == null) {
//							for(int i=0;i<result.size();i++){
//								ParseObject item=result.get(i);
//								ItemClass itclass=new ItemClass();
//								itclass.name=item.getString("fish_name");
//								try{
//								itclass.img=item.getParseFile("fish_img").getUrl();
//								}
//								catch(Exception e1){
//									
//								}
//								itclass.obid=item.getObjectId();
//								itclass.description=item.getString("fish_desc");
//								itclass.freshnes_scale=item.getString("freshness_scale");
//								itclass.processing_charge=item.getNumber("processing_charge").toString();
//								itclass.bestfor=item.getString("best_for");
//								itclass.price=item.getNumber("unit_price").toString();
//								itclass.uom_type=item.getString("uom");
//								
//								ArrayList<String> processings=new  ArrayList<String>();
//								for(int j=0;j<item.getJSONArray("processings").length();j++)
//								{
//									try {
//										processings.add(item.getJSONArray("processings").getString(j));
//									} catch (JSONException e1) {
//										// TODO Auto-generated catch block
//										e1.printStackTrace();
//									}
//								}
//								itclass.processings=processings;
//								contactAdapter.add(itclass);
//								Log.e("retrieved object", item.toString());
//							}
//						
//						}
//						else {
//							Log.e("error while loading", e.toString());
//						}
//						
//					
//					
//					}
//					
//		
//		});
//		
//	}
	@Override
	protected void onResume() {

		super.onResume();

	
	}
	
	@Override
	public void onBackPressed() {
		finish();
		overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
	}
}
