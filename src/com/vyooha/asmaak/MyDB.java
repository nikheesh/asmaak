package com.vyooha.asmaak;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

public class MyDB {

	private MyDatabaseHelper dbHelper;

	private SQLiteDatabase database;


	public MyDB(Context context) {
		dbHelper = new MyDatabaseHelper(context);
		database = dbHelper.getWritableDatabase();
		}

//	"create table Cart(fishid text,isclean text,processings integer,quantity integer,price integer,imgurl text);";
	public long Add_Cart(String fishid,String fishname,boolean isCleaningRequiered,String processings,int quantity,float unit_price,float cleaningCharge,String imageurl,String uom) {

		ContentValues values = new ContentValues();

		values.put("fishid", fishid);
		values.put("fishname", fishname);
		values.put("isclean", Boolean.toString(isCleaningRequiered));
		values.put("processings", processings);
		values.put("quantity", quantity);
		values.put("imgurl", imageurl);
		values.put("uom", uom);
		values.put("cleaningcharge",cleaningCharge);
		
		values.put("price", unit_price);

		long l = database.insert("Cart", null, values);

		return l;
	}
	
	public Cursor selectCart() {
		 String[] cols = new String[]
		 {"fishid","fishname","isclean","processings","quantity","price","cleaningcharge","imgurl","uom"
		 };

		Cursor mCursor = database.query(true, "Cart", cols,
				null, null, null, null, null, null);
		if (mCursor != null) {
			mCursor.moveToFirst();
		}
		return mCursor;
		// iterate to get each value.
	}
	
	public Cursor selectCartByfishid(String fishid) {
		 String[] cols = new String[]
		 {"fishid"
		 };
			
		 String whereClause = "fishid=?";
		String[] whereArgs = new String[] {fishid};

		Cursor mCursor = database.query(true, "Cart", null,
				whereClause, whereArgs, null, null, null, null);
		if (mCursor != null) {
			mCursor.moveToFirst();
		}
		return mCursor;
		// iterate to get each value.
	}
	
	
	public void updateCartQuantityPriceUom(String fishid,boolean isCleaningRequiered,String processings,int quantity,float unit_price,float cleaningCharge,String uom){
		
		ContentValues cv = new ContentValues();
		cv.put("quantity", quantity);
		cv.put("price", unit_price);
		cv.put("uom", uom);
		cv.put("cleaningcharge", cleaningCharge);
		cv.put("isclean", Boolean.toString(isCleaningRequiered));
		cv.put("processings", processings);
		
		String whereClause = "fishid=?";
		String[] whereArgs = new String[] {fishid};
		database.update("Cart", cv, whereClause, whereArgs);
		
		
	}
public void updateCartQuantity(String fishid,int quantity){
		
		ContentValues cv = new ContentValues();
		cv.put("quantity", quantity);
		
		String whereClause = "fishid=?";
		String[] whereArgs = new String[] {fishid};
		database.update("Cart", cv, whereClause, whereArgs);
		
		
	}
	public void deleterow_Cart(String fishid) {
		// Log.d("delete row got executed", "delete");

			String whereClause = "fishid=? ";
			String[] whereArgs = new String[] { fishid };
			database.delete("Cart", whereClause, whereArgs);
	}

	public void deleteall_Cart() {
		// Log.d("delete row got executed", "delete");

		
			database.delete("Cart",null, null);
	}


	public void droptable_Cart() {
	//	Log.d("drop got executed", "drop");
		database.execSQL("DROP TABLE Cart");

	}

	public void close() {
		// Log.d("database", "db closed");
		database.close();
	}

	public void createtable_Cart() {
		//Log.d("create got executed", "create");
		database.execSQL("create table Cart(fishid text,isclean text,processings integer,quantity integer,imgurl text);");
	}
}