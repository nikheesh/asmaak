package com.vyooha.asmaak;

import java.nio.channels.SelectableChannel;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.parse.FunctionCallback;
import com.parse.ParseCloud;
import com.parse.ParseException;
import com.parse.ParseObject;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.view.KeyEvent;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class ShopActivity extends Activity {
	
	RelativeLayout rl_freshfish,rl_allfish,rl_shellfish,rl_recipies;
	TextView txt_nofreshfish,txt_noallfish,txt_noshellfish;
	TextView txt_bestfried,txt_bestgrilled,txt_bestovened,txt_about,txt_delivery,txt_contact;
	ImageButton imgbtn_option,imgbtn_cart;
	//EditText edit_search;
	TextView txt_head;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState); 
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
				WindowManager.LayoutParams.FLAG_FULLSCREEN);

		setContentView(R.layout.activity_shop);
		
		rl_allfish=(RelativeLayout)findViewById(R.id.rl_shop_allfish);
		rl_freshfish=(RelativeLayout)findViewById(R.id.rl_shop_freshfish);
		rl_shellfish=(RelativeLayout)findViewById(R.id.rl_shop_shellfish);
		rl_recipies=(RelativeLayout)findViewById(R.id.rl_shop_recipies);
		
		txt_noallfish=(TextView)findViewById(R.id.txt_shop_noAllfish);
		txt_nofreshfish=(TextView)findViewById(R.id.txt_shop_nofreshfish);
		txt_noshellfish=(TextView)findViewById(R.id.txt_shop_noShellfishes);
		
		txt_bestfried=(TextView)findViewById(R.id.txt_shop_bestfried);
		txt_bestgrilled=(TextView)findViewById(R.id.txt_shop_bestgrilled);
		txt_bestovened=(TextView)findViewById(R.id.txt_shop_bestovened);
		
		txt_about=(TextView)findViewById(R.id.txt_shop_aboutus);
		txt_delivery=(TextView)findViewById(R.id.txt_shop_delivery);
		txt_contact=(TextView)findViewById(R.id.txt_shop_contacts);
		
//		edit_search=(EditText)findViewById(R.id.edit_common_search);
//		edit_search.setVisibility(View.GONE);
		txt_head=(TextView)findViewById(R.id.txt_common_head);
		
		
		imgbtn_option=(ImageButton)findViewById(R.id.imgbtn_common_option);
		imgbtn_option.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
			Intent i=new Intent(getApplicationContext(), SigninSignUpFavourite_Activity.class);
			startActivity(i);
			overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
			}
		});
		
//		
		imgbtn_cart=(ImageButton)findViewById(R.id.imgbtn_common_search);
//		imgbtn_cart.setOnClickListener(new OnClickListener() {
//			
//			@Override
//			public void onClick(View v) {
//			Intent i=new Intent(getApplicationContext(),ItemListCustomer_Activity.class);
//			i.putExtra("category", "all");
//			startActivity(i);
//			}
//		});
//		
//		
imgbtn_cart.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Intent intent=new Intent(getApplicationContext(), CartListCustomer_Activity.class);
				startActivity(intent);
				overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
//				edit_search.setVisibility(View.VISIBLE);
//				txt_head.setVisibility(View.GONE);
			}
		});
		
//		edit_search.setOnEditorActionListener(new TextView.OnEditorActionListener() {
//		    @Override
//		    public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
//		        if (actionId == EditorInfo.IME_ACTION_SEARCH) {
//		        	
//		        	String tag=edit_search.getText().toString();
//		           
//		        	
//		        	
//		        	InputMethodManager imm = (InputMethodManager) getSystemService(
//		    				Context.INPUT_METHOD_SERVICE);
//		    		imm.hideSoftInputFromWindow(edit_search.getWindowToken(), 0);
//		    		
//		    		Intent i=new Intent(getApplicationContext(), ItemListCustomer_Activity.class);
//		    		i.putExtra("category", tag);
//		    		startActivity(i);
//		            edit_search.setVisibility(View.GONE);
//		        	txt_head.setVisibility(View.VISIBLE);
//		    		
//		            return true;		            
//		        }
//		        return false;
//		    }
//		}); 
//		
		
		HashMap<String, String> input_param = new HashMap<String, String>();
		
		
		ParseCloud.callFunctionInBackground("getItemCountByCategory", input_param,
				new FunctionCallback<Map<String,String>>() {

					@Override
					public void done(Map<String,String> result,
							ParseException e) {
						// TODO Auto-generated method stub
						if (e == null) {
							txt_noallfish.setText(result.get("all").toString()+" ITEMS");
							txt_nofreshfish.setText(result.get("fresh").toString()+" ITEMS");
							txt_noshellfish.setText(result.get("shell").toString()+" ITEMS");
							
							txt_noallfish.setVisibility(View.VISIBLE);
							txt_nofreshfish.setVisibility(View.VISIBLE);
							txt_noshellfish.setVisibility(View.VISIBLE);
							
						}
						
					
					}});
	
		
		rl_allfish.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				selctedview("all");
			}
		});
		
		rl_freshfish.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
			selctedview("fresh");	
			}
		});
		
		rl_shellfish.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
			selctedview("shell");	
			}
		});
		
		
		txt_bestfried.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				selctedview("fried");
			}
		});
		txt_bestgrilled.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				selctedview("grilled");
			}
		});
		
		txt_bestovened.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
			selctedview("ovened");	
			}
		});
		
		rl_recipies.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Intent i=new Intent(getApplicationContext(),Recipies_Activity.class);
				
				startActivity(i);
				
				overridePendingTransition(R.anim.push_up_in, R.anim.push_down_out);
					
			}
		});
		
		txt_about.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Intent i=new Intent(getApplicationContext(),About_Activity.class);
				
				startActivity(i);
				
				overridePendingTransition(R.anim.push_up_in, R.anim.push_down_out);
					
			}
		});
		txt_contact.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Intent i=new Intent(getApplicationContext(),Contacts_Activity.class);
				
				startActivity(i);
				
				overridePendingTransition(R.anim.push_up_in, R.anim.push_down_out);
					
			}
		});
		
		txt_delivery.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Intent i=new Intent(getApplicationContext(),Delivery_Activity.class);
				
				startActivity(i);
				
				overridePendingTransition(R.anim.push_up_in, R.anim.push_down_out);
					
			}
		});
		
	}
	
public	void selctedview(String category) {
	
	Intent i=new Intent(getApplicationContext(),ItemListCustomer_Activity.class);
	i.putExtra("category", category);
	startActivity(i);
	
	overridePendingTransition(R.anim.push_up_in, R.anim.push_down_out);
	
}
@Override
public void onBackPressed() {
	finish();
	}
}
