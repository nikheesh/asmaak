package com.vyooha.asmaak;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.webkit.WebView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

public class Recipies_Activity extends Activity{
	WebView txt_html;
	ImageButton img_back;
	
//	ApplicationClass appState;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_recipies);
		
		txt_html=(WebView)findViewById(R.id.txt_about_abttxt);
		txt_html.loadUrl("file:///android_asset/recipie.html");
		
		img_back=(ImageButton)findViewById(R.id.imgbtn_help_backbtn);
		img_back.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				finish();
				overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
			}
		});
		
		
	}
	
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
	    if(event.getAction() == KeyEvent.ACTION_DOWN){
	        switch(keyCode)
	        {
	        case KeyEvent.KEYCODE_BACK:
	            if(txt_html.canGoBack()){
	                txt_html.goBack();
	            }else{
	                finish();
	                overridePendingTransition(R.anim.slide_out_left, R.anim.slide_out_right);

	            }
	            return true;
	        }

	    }
	    return super.onKeyDown(keyCode, event);
	}

	
	
	
	
}
