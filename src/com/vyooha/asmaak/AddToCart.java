package com.vyooha.asmaak;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.parse.FunctionCallback;
import com.parse.ParseCloud;
import com.parse.ParseException;
import com.parse.ParseUser;

import android.R.string;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.View.OnClickListener;
import android.view.View.OnFocusChangeListener;
import android.view.View.OnSystemUiVisibilityChangeListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.Toast;

@SuppressLint("NewApi")
public class AddToCart	extends Activity {
	
	TextView txt_name,txt_freshness,txt_price,txt_processingcharge,txt_description,txt_head;
	TextView edit_qty;
	//Spinner cleaningstatus;
	Button btn_addcart;
	ImageView item_img;
	ImageButton img_back;
	
	TextView txt_favourite_this;
	
	// for quantity increment
	Button btn_add_qty,btn_sub_qty;
	int quantity=1;
	
	Spinner spinner_processingtype;
	String processtype="";
	private ArrayList<String> processingslist = new ArrayList<String>();
	private ArrayAdapter<String> processingadapter;
	
	InputMethodManager imm;
	
	
	
	final DisplayImageOptions options = new DisplayImageOptions.Builder()
	.cacheInMemory(true).cacheOnDisc(true)
	.resetViewBeforeLoading(true)
	// .showImageForEmptyUri(fallback)
	// .showImageOnFail(fallback)
	// .showImageOnLoading(fallback)
	.build();
	
	ParseUser user;
	MyDB db;
	@SuppressLint("NewApi")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState); 
		
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);
		
		setContentView(R.layout.activity_addtocart);
		
		user=ParseUser.getCurrentUser();
		db=new MyDB(getApplicationContext());
		
		txt_name=(TextView)findViewById(R.id.txt_addtocart_name);
		txt_freshness=(TextView)findViewById(R.id.txt_addtocart_freshness);
		txt_price=(TextView)findViewById(R.id.txt_addtocart_price);
		txt_processingcharge=(TextView)findViewById(R.id.txt_addtocart_processingcharge);
		txt_description=(TextView)findViewById(R.id.txt_addtocart_description);
		txt_favourite_this=(TextView)findViewById(R.id.txt_addtocart_favouritethis);
		txt_head=(TextView)findViewById(R.id.txt_common_headtxt);
		img_back=(ImageButton)findViewById(R.id.imgbtn_common_back);
		
		//edit_address=(EditText)findViewById(R.id.edit_addtocart_address);
		edit_qty=(TextView)findViewById(R.id.edit_addtocart_quantity);
		item_img=(ImageView)findViewById(R.id.img_addtocart_itemimg);
		//cleaningstatus=(Spinner)findViewById(R.id.spinner_addtocart_cleaning);
		

		
		
		
		

			
		btn_add_qty=(Button)findViewById(R.id.btn_addtocart_add);
		btn_sub_qty=(Button)findViewById(R.id.btn_addtocart_sub);
		
		btn_addcart=(Button)findViewById(R.id.btn_addtocart_addtocartbtn);
		
		spinner_processingtype=(Spinner)findViewById(R.id.spinner_addtocart_cleaning);
		
		processingslist=getIntent().getStringArrayListExtra("processings");
		processtype=processingslist.get(0);
		
		processingadapter=new ArrayAdapter<String>(getApplicationContext(), R.layout.spinner_item2,processingslist);
		spinner_processingtype.setAdapter(processingadapter);
		spinner_processingtype.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> adapterview, View view,
					int position, long id) {
		
				processtype=(String)adapterview.getItemAtPosition(position);

			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
				// TODO Auto-generated method stub

			}
		});
		
		
		
		btn_add_qty.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
			quantity++;
			edit_qty.setText(quantity+"");
			}
		});
		
		btn_sub_qty.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if(quantity>1)
					quantity--;
				edit_qty.setText(quantity+"");
			}
		});
		
		img_back.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
		finish();
		}
		});
		

	//	i.putExtra("name", it.name);
//		i.putExtra("cleaningcharge", it.cleaning_charge);
//		i.putExtra("freshness_scale", it.freshnes_scale);
//		i.putExtra("price", it.price);
//		i.putExtra("itemid", it.obid);
	//i.putExtra("imageurl",it.img );	
	final	String name=getIntent().getStringExtra("name");
	final	String process_chg=getIntent().getStringExtra("processingcharge");
	final	String freshness=getIntent().getStringExtra("freshness_scale");
	final	String price=getIntent().getStringExtra("price");
	final	String itemid=getIntent().getStringExtra("itemid");
	final String imgurl=getIntent().getStringExtra("imageurl");
	final String uom=getIntent().getStringExtra("uom");
	final String fish_desc=getIntent().getStringExtra("fish_desc");
	
	ImageLoader  imgloader=ImageLoader.getInstance();
		imgloader.displayImage(imgurl, item_img);
	//txt_cleaning.setText(clean_chg);
	txt_freshness.setText(freshness);
	txt_processingcharge.setText(process_chg);
	txt_name.setText(name.toUpperCase());
	txt_price.setText("QR "+price+"/"+uom);
	txt_description.setText(fish_desc);
	txt_head.setText(name.toUpperCase());
	
	txt_favourite_this.setOnClickListener(new OnClickListener() {
		
		@Override
		public void onClick(View v) {
		

			Map<String, Object> inputparm = new HashMap<String, Object>();
			
						
						inputparm.put("fish_obj", itemid);
						
			
							ParseCloud.callFunctionInBackground("addToFav", inputparm,
									new FunctionCallback<String>() {
			
										@Override
										public void done(String result,
												ParseException e) {
											// TODO Auto-generated method stub
											if (e == null) {
											
											Toast.makeText(getApplicationContext(), result, Toast.LENGTH_LONG).show();
											}
											
										
										}});
			
		}
	});
	
	
		btn_addcart.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				boolean iscleaningRequiered=true;
				if(processtype.equals("not cleaning") || processtype.equalsIgnoreCase("not cleaning")){
					iscleaningRequiered=false;
				}
				
				try{
				quantity=Integer.parseInt(edit_qty.getText().toString());
				}
				catch(Exception e){
					
				}
				
				
				
		if(user==null)		
		{
			Cursor c=db.selectCartByfishid(itemid);
			float unit_price=1;
			try{
				unit_price=Float.parseFloat(price);
				}
			catch(Exception e){
			
				}
			float cleaningcharge=1;
			try{
				cleaningcharge=Float.parseFloat(process_chg);
			}
			catch(Exception e){
				
			}
			
			if(c.getCount()>0){
				
				db.updateCartQuantityPriceUom(itemid,iscleaningRequiered,processtype, quantity, unit_price,cleaningcharge,uom);
			}
			else
				db.Add_Cart(itemid,name, iscleaningRequiered, processtype, quantity,unit_price,cleaningcharge, imgurl,uom);
			Toast.makeText(getApplicationContext(), "Fish added to your cart", Toast.LENGTH_LONG).show();
//			
		finish();
		
		}
		else{
			
			
			
			Map<String, Object> inputparm = new HashMap<String, Object>();
			
						inputparm.put("quantity", quantity);
						inputparm.put("fish_id", itemid);
						Log.e("itemid", itemid);
						inputparm.put("isCleanRequired", iscleaningRequiered);
						ArrayList<String> processings=new ArrayList<String>();
						processings.add(processtype);
						inputparm.put("processings", processings);
			
							ParseCloud.callFunctionInBackground("addToCart", inputparm,
									new FunctionCallback<String>() {
			
										@Override
										public void done(String result,
												ParseException e) {
											// TODO Auto-generated method stub
											if (e == null) {
												finish();
												Toast.makeText(getApplicationContext(), result, Toast.LENGTH_LONG).show();
//												Intent i=new Intent(getApplicationContext(), CartListCustomer_Activity.class);
//												startActivity(i);
//												
											}
											else{
												Toast.makeText(getApplicationContext(),"Can't add this item to your cart now.", Toast.LENGTH_LONG).show();
											}
										}});
			}
				
			
		
	}});
		
		
		
		
	}
}
