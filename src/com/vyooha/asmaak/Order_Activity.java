package com.vyooha.asmaak;

import java.io.ObjectOutputStream.PutField;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.parse.FunctionCallback;
import com.parse.ParseCloud;
import com.parse.ParseException;
import com.parse.ParseUser;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

public class Order_Activity	extends Activity {
	
	
	Button order_main,order_sub;
	EditText edit_cardno,edit_cvv;
	Spinner spinner_month,spinner_year;
	float totalforpayment=0;
	MyDB db;
	ParseUser user;
	AsmaakApplication appState;
	private ArrayList<Integer> monthlist,yearlist;
	private ArrayAdapter<Integer> monthadapter,yearadapter;
	
	public  ProgressDialog proDialog;
	public  void startLoading() {
	    proDialog = new ProgressDialog(this);
	    proDialog.setMessage(" Please Wait.....");
	    proDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
	    proDialog.setCancelable(false);
	    proDialog.show();
	}

public  void stopLoading() {
	    proDialog.dismiss();
	    proDialog = null;
	}
	
	final DisplayImageOptions options = new DisplayImageOptions.Builder()
	.cacheInMemory(true).cacheOnDisc(true)
	.resetViewBeforeLoading(true)

	.build();
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState); 
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);
		
		setContentView(R.layout.activity_order);
		user=ParseUser.getCurrentUser();
		db=new MyDB(getApplicationContext());
		appState=(AsmaakApplication)getApplicationContext();
			
				edit_cardno=(EditText)findViewById(R.id.edit_order_cardno);
				edit_cvv=(EditText)findViewById(R.id.edit_order_cvv);
			
		
		
			order_main=(Button)findViewById(R.id.btn_order_paybtn_main);
			order_sub=(Button)findViewById(R.id.btn_order_paybtn_head);
			order_main.setText("pay QR "+getIntent().getFloatExtra("totalprice", 0));
			order_sub.setText("pay QR "+getIntent().getFloatExtra("totalprice", 0));
			
			monthlist=new ArrayList<Integer>();
			yearlist=new ArrayList<Integer>();
			
			spinner_month=(Spinner)findViewById(R.id.spinner_order_expirymonth);
			spinner_year=(Spinner)findViewById(R.id.spinner_order_expiryyear);
			
			for(int i=1;i<12;i++){
				monthlist.add(i);
			}
			for (int i = 2015; i < 2050; i++) {
				yearlist.add(i);
			}
			
			monthadapter=new ArrayAdapter<Integer>(getApplicationContext(), R.layout.spinner_item, monthlist);
			yearadapter=new ArrayAdapter<Integer>(getApplicationContext(), R.layout.spinner_item, yearlist);
			
			
			spinner_month.setAdapter(monthadapter);
			spinner_year.setAdapter(yearadapter);
		 totalforpayment=getIntent().getFloatExtra("totalprice", 0);
			order_main.setText("pay QR "+totalforpayment);
			order_sub.setText("pay QR "+totalforpayment);
			
			order_main.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				
				if(user!=null){
					orderforUser();
				}
				else{
					orderforguest();
				}
			}
		});
			
			order_sub.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
				order_main.performClick();	
				}
			});
		
		
		
		
	}
	
	public void orderforguest(){
		
	
		
		String jsonstr=appState.pref.getString("orderhead", "0");
		JSONObject json1=new JSONObject();
		if(jsonstr!=null)
			try {
				json1=new JSONObject(jsonstr);
			} catch (JSONException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		
//		json.put("address", txt_address.getText().toString());
//		json.put("phone", txt_phone.getText().toString());
//		json.put("deliveryslot", txt_arrivetime.getText().toString());
//		json.put("comments", txt_comment.getText().toString());
//		
		 String phone="",address="",deliveryslot="",comment="";
		try{
		 phone=json1.getString("phone");
		 address=json1.getString("address");
		 deliveryslot=json1.getString("deliveryslot");
		 comment=json1.getString("comments");
		}
		catch(Exception e){
			
		}
			HashMap<String, Object> inputparm = new HashMap<String, Object>();
			JSONObject jsonhead=new JSONObject();
			try{
			jsonhead.put("phone", phone);
			jsonhead.put("address", address);
			jsonhead.put("delivery_slot", deliveryslot);
			jsonhead.put("comment", comment);
			}
			catch(Exception e){
				
			}
			
				try{
				jsonhead.put("guest_email",getIntent().getStringExtra("email"));
				
				}
				catch(Exception e){
					
				}
				JSONArray jsonarraydetails=new JSONArray();
				
				Cursor c=db.selectCart();
				for(int i=0;i<c.getCount();i++){
					c.moveToPosition(i);
					JSONObject json=new JSONObject();
					try{
					json.put("fish_id", c.getString(0));
					json.put("isCleanRequired", c.getString(2));
					json.put("quantity", c.getInt(4));
					json.put("processings", c.getString(3));
					}
					catch(Exception e){
						
					}
					
					jsonarraydetails.put(json);
				}
				
				
				startLoading();
				
				inputparm.put("order_header", jsonhead);
				inputparm.put("order_items", jsonarraydetails);
				ParseCloud.callFunctionInBackground("placeOrderAsGuest", inputparm,
						new FunctionCallback<String>() {

				@Override
				public void done(String result,
						ParseException e) {
					// TODO Auto-generated method stub
					if (e == null) {
						if(proDialog!=null)
							stopLoading();
						finish();
						Intent i=new Intent(getApplicationContext(), ShopActivity.class);
						startActivity(i);
						appState.editor.remove("orderhead");
						appState.editor.commit();
						db.deleteall_Cart();
						Toast.makeText(getApplicationContext(), result, Toast.LENGTH_LONG).show();
					}
					else{

						if(proDialog!=null)
							stopLoading();
						Toast.makeText(getApplicationContext(), "can't make order now", Toast.LENGTH_LONG).show();
					}
					
				}});
				
	}
	
	
public void orderforUser(){
		
		String jsonstr=appState.pref.getString("orderhead", "0");
		JSONObject json=new JSONObject();
		if(jsonstr!=null)
			try {
				json=new JSONObject(jsonstr);
			} catch (JSONException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		
//		json.put("address", txt_address.getText().toString());
//		json.put("phone", txt_phone.getText().toString());
//		json.put("deliveryslot", txt_arrivetime.getText().toString());
//		json.put("comments", txt_comment.getText().toString());
//		
		 String phone="",address="",deliveryslot="",comment="";
		try{
		 phone=json.getString("phone");
		 address=json.getString("address");
		 deliveryslot=json.getString("deliveryslot");
		 comment=json.getString("comments");
		}
		catch(Exception e){
			
		}
			HashMap<String, Object> inputparm = new HashMap<String, Object>();
			
				
				
				inputparm.put("phone", phone);
				inputparm.put("delivery_slot", deliveryslot);
				inputparm.put("address", address);
				inputparm.put("comment", comment);
			startLoading();
				ParseCloud.callFunctionInBackground("placeOrderForUser", inputparm,
						new FunctionCallback<String>() {

				@Override
				public void done(String result,
						ParseException e) {
					// TODO Auto-generated method stub
					if (e == null) {
						

						if(proDialog!=null)
							stopLoading();
						
						finish();
						Intent i=new Intent(getApplicationContext(), ShopActivity.class);
						startActivity(i);

						appState.editor.remove("orderhead");
						appState.editor.commit();
						Toast.makeText(getApplicationContext(), result, Toast.LENGTH_LONG).show();
					}
					else{

						if(proDialog!=null)
							stopLoading();
						Toast.makeText(getApplicationContext(), "can't make order now", Toast.LENGTH_LONG).show();
						
					}
					
				}});
			

}
		
		
				
			
			
	
	
	
}
