package com.vyooha.asmaak;




import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;

import com.parse.FunctionCallback;
import com.parse.LogInCallback;
import com.parse.ParseCloud;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseUser;
import com.parse.SignUpCallback;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences.Editor;
import android.database.Cursor;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;

public class LoginAndSignup_Activity  extends Activity {
	//declaration of variables
	//TextView forgotpass_txtview,createnew_txtview;
	EditText signin_email_edit,signin_password_edit;
	Button signin_btn;
	Intent intent;
	String email,pass,name,mobile;
	ImageButton imgbtn_back;
	EditText signup_email_edit,signup_password_edit,signup_name_edit,signup_mobile_edit;
	Button signup_btn;
	
	private Handler mHandler = new Handler();
	private int media_position = 1;
	public  ProgressDialog proDialog;
	public  void startLoading() {
	    proDialog = new ProgressDialog(this);
	    proDialog.setMessage("Signing in. Please Wait.....");
	    proDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
	    proDialog.setCancelable(false);
	    proDialog.show();
	}

public  void stopLoading() {
	    proDialog.dismiss();
	    proDialog = null;
	}
	
	
	 boolean validationError;
	 String errorMessage;
	
	 boolean isSelectedSignIn=false;
	 Button signuplayout_btn,signinlayout_btn;
	 LinearLayout ll_signup;
	 RelativeLayout rl_signin;
	 MyDB db;
	 @Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
			requestWindowFeature(Window.FEATURE_NO_TITLE);
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);
		
		setContentView(R.layout.activity_signup);
		db=new MyDB(getApplicationContext());
		//declaratio for sign in 
		signin_email_edit=(EditText)findViewById(R.id.edit_signin_email);
		signin_password_edit=(EditText)findViewById(R.id.edit_signin_password);
		signin_btn=(Button)findViewById(R.id.btn_signin_signin);
		imgbtn_back=(ImageButton)findViewById(R.id.imgbtn_common_back);
		
		
		
		//declaration for signup
		
		signup_btn=(Button)findViewById(R.id.btn_signup_signup);
		signup_email_edit=(EditText)findViewById(R.id.edit_signup_email);
		signup_mobile_edit=(EditText)findViewById(R.id.edit_signup_mobile);
		signup_name_edit=(EditText)findViewById(R.id.edit_signup_name);
		signup_password_edit=(EditText)findViewById(R.id.edit_signup_password);
		
//		createnew_txtview=(TextView)findViewById(R.id.txt_signin_signup);
//		forgotpass_txtview=(TextView)findViewById(R.id.txt_signin_forgot);
		signuplayout_btn=(Button)findViewById(R.id.btn_desicion_signup);
		signinlayout_btn=(Button)findViewById(R.id.btn_decision_signin);
		ll_signup=(LinearLayout)findViewById(R.id.ll_signup_signuplayout);
		rl_signin=(RelativeLayout)findViewById(R.id.rl_signin_signinlayout);
		
		signin_password_edit.setTypeface(Typeface.DEFAULT);
		signin_password_edit.setTransformationMethod(new PasswordTransformationMethod());
		
		// first hide or visible the signin/signup button
		imgbtn_back.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
		finish();		
			}
		});
		
		
		signuplayout_btn.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
			
				signuplayout_btn.setBackgroundColor(Color.parseColor("#036796"));
				signuplayout_btn.setTextColor(Color.WHITE);
				
				signinlayout_btn.setBackgroundColor(Color.WHITE);
				signinlayout_btn.setTextColor(Color.parseColor("#036796"));
				
				ll_signup.setVisibility(View.VISIBLE);
				rl_signin.setVisibility(View.GONE);
				
				isSelectedSignIn=false;
			}
		});
		
		
		signinlayout_btn.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
			
				signinlayout_btn.setBackgroundColor(Color.parseColor("#036796"));
				signinlayout_btn.setTextColor(Color.WHITE);
				
				signuplayout_btn.setBackgroundColor(Color.WHITE);
				signuplayout_btn.setTextColor(Color.parseColor("#036796"));
				
				rl_signin.setVisibility(View.VISIBLE);
				ll_signup.setVisibility(View.GONE);
				
				isSelectedSignIn=true;
			}
		});
		
		
		
		
		
		
		
		signin_btn.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				
				email=signin_email_edit.getText().toString();
				pass=signin_password_edit.getText().toString();
				if(email.length()!=0&&pass.length()!=0){
					if(Utilities_Functions.chekEmailId(email)){
					
				if(Utilities_Functions.checkinternet(getApplicationContext())){
					startLoading();
			ParseUser.logInInBackground(email, pass, new LogInCallback() {
				
				@Override
				public void done(ParseUser user, ParseException e) {
					// TODO Auto-generated method stub
					if(e==null){
						
						if(getIntent().getStringExtra("time").equals("order")){

						JSONArray jsonarray=new JSONArray();
						
						
						Cursor c=db.selectCart();
						
						for(int i=0;i<c.getCount();i++){
							c.moveToPosition(i);
							JSONObject json=new JSONObject();
							// {"fishid","fishname","isclean","processings","quantity","price","cleaningcharge","imgurl","uom"
							try{
							json.put("fish_id", c.getString(0));
							json.put("isCleanRequired", c.getString(2));
							json.put("fish_quan", c.getInt(4));
							ArrayList<String> processings=new ArrayList<String>();
							processings.add(c.getString(3));
							json.put("processings",processings);
							}
							catch (Exception e1){
								
							}
							jsonarray.put(json);
						}
						
						HashMap<String, Object> input_param = new HashMap<String, Object>();
						input_param.put("fish_array", jsonarray);
				
						
						//This function not defined on cloud
						
						
						ParseCloud.callFunctionInBackground("addManyToCart", input_param,
								new FunctionCallback<String>() {

									@Override
									public void done(String result,
											ParseException e) {
										// TODO Auto-generated method stub
										if (e == null) {
											if(proDialog!=null)
												stopLoading();
											db.deleteall_Cart();
											finish();
											Intent i=new Intent(getApplicationContext(),Confirm_Activity.class);
											i.putExtra("type", "user");
											
											i.putExtra("totalprice", getIntent().getFloatExtra("totalprice", 0));
											startActivity(i);
										}
										else{
											if(proDialog!=null)
												stopLoading();
											Toast.makeText(getApplicationContext(), "can't added to cart now", Toast.LENGTH_LONG).show();
										}
									}});
						
					}
						
						else{

							if(proDialog!=null)
								stopLoading();
							finish();
							Toast.makeText(getApplicationContext(), "successfully signed in", Toast.LENGTH_LONG).show();
							Intent i=new Intent(getApplicationContext(),ShopActivity.class);
							startActivity(i);
						}
			
						
					}
					else{
						
						if(proDialog!=null)
							stopLoading();
						Toast.makeText(getApplicationContext(), "can't sign in now", Toast.LENGTH_LONG).show();
					
					}
					
					
				}
			});
				}
				else{

					if(proDialog!=null)
						stopLoading();
					Toast.makeText(getApplicationContext(), "Please check your internet connection", Toast.LENGTH_LONG).show();
				}
				
				
					}
					else {

						if(proDialog!=null)
							stopLoading();
						Toast.makeText(getApplicationContext(), "Enter a valid email id", Toast.LENGTH_LONG).show();
						
					}
		}
			else{

				if(proDialog!=null)
					stopLoading();
				Toast.makeText(getApplicationContext(), "Please add all fields", Toast.LENGTH_LONG).show();
			}
			}
		});
	
		
		signup_btn.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				email=signup_email_edit.getText().toString();
				pass=signup_password_edit.getText().toString();
				name=signup_name_edit.getText().toString();
				mobile=signup_mobile_edit.getText().toString();
	
				
				 boolean validationError = false;
				    int c=0;
				    
				  String errorMessage="";
				  if (name.length() == 0) {
				    	c++;
				    	
				        validationError = true;
				        errorMessage="Enter Your name";
				    }
				    if ( email.length() == 0) {
				    	validationError = true;
				    	 if(c==0){
					    	  errorMessage="Enter email";
					    	}
					      c++;
					    	
				    	
				      }
				    else 
				    	
				    	{
				    	Log.e("mmmmmmmmm",email.matches("^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@ [A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$")+"");
				    	if(Utilities_Functions.chekEmailId(email)==false)
				           {
				    	validationError = true;
				    	 if(c==0){
					    	  errorMessage="Enter a valid email address";
					    	}
					      c++;
				           }
				    	}
				    if (pass.length() == 0) {
					      
					      validationError = true;
					      if(c==0){
					    	  errorMessage="Enter a valid password";
					    	}
					      c++;
					      
					    }
					    if (mobile.length() == 0) {
						      validationError = true;
						      if(c==0){
						    	  errorMessage="Enter your mobile number";
						    	}
						      c++;
						      
						    }
				  if (validationError) {
				     Toast.makeText(getApplicationContext(), errorMessage, Toast.LENGTH_LONG).show();
				      return;
				    }
				
				
				
				
					
				if(Utilities_Functions.checkinternet(getApplicationContext())){
			ParseUser user=new ParseUser();
			user.setUsername(email);
			user.setEmail(email);
			user.setPassword(pass);
			user.put("phone", mobile);
			user.put("name", name);
			user.put("user_type", "user");
			startLoading();
				user.signUpInBackground(new SignUpCallback() {
					
					@Override
					public void done(ParseException e) {
						
						if(e==null){
							if(getIntent().getStringExtra("time").equals("order"))
							{
							JSONArray jsonarray=new JSONArray();
							
							
							Cursor c=db.selectCart();
							
							for(int i=0;i<c.getCount();i++){
								c.moveToPosition(i);
								JSONObject json=new JSONObject();
								// {"fishid","fishname","isclean","processings","quantity","price","cleaningcharge","imgurl","uom"
								try{
								json.put("fish_id", c.getString(0));
								json.put("isCleanRequired", c.getString(2));
								json.put("fish_quan", c.getInt(4));
								ArrayList<String> processings=new ArrayList<String>();
								processings.add(c.getString(3));
								json.put("processings",processings);
								}
								catch (Exception e1){
									
								}
								jsonarray.put(json);
							}
							
							HashMap<String, Object> input_param = new HashMap<String, Object>();
							input_param.put("fish_array", jsonarray);
					
							
							//This function not defined on cloud
							
							
							ParseCloud.callFunctionInBackground("addManyToCart", input_param,
									new FunctionCallback<String>() {

										@Override
										public void done(String result,
												ParseException e) {
											// TODO Auto-generated method stub
											if (e == null) {
												if(proDialog!=null)
													stopLoading();
												db.deleteall_Cart();
												finish();
												Intent i=new Intent(getApplicationContext(),Confirm_Activity.class);
												i.putExtra("type", "user");
												
												i.putExtra("totalprice", getIntent().getFloatExtra("totalprice", 0));
												startActivity(i);
											}
											else{

												if(proDialog!=null)
													stopLoading();
											Toast.makeText(getApplicationContext(), "can't added items into your cart", Toast.LENGTH_LONG).show();
											}
											
										}});
							
							}
							else{
								finish();

								if(proDialog!=null)
									stopLoading();
								Toast.makeText(getApplicationContext(), "Successfully signed in", Toast.LENGTH_LONG).show();
								Intent i=new Intent(getApplicationContext(),ShopActivity.class);
								startActivity(i);
							}
							
							
						}
						
						else{
							if(proDialog!=null)
								stopLoading();
							Toast.makeText(getApplicationContext(), "Successfully signed in", Toast.LENGTH_LONG).show();
							
						}
						
					}
				});
				
				
				
				
				}
				else{

					if(proDialog!=null)
						stopLoading();
					Toast.makeText(getApplicationContext(), "Please check your internet connection", Toast.LENGTH_LONG).show();
				}
				
				
					
			}
		});
		
		
//		
		
		//create new account
//		createnew_txtview.setOnClickListener(new OnClickListener() {
//			
//			@Override
//			public void onClick(View v) {
//
////				intent=new Intent(getApplicationContext(),CreateNew_Activity.class);
////				startActivity(intent);
//			}
//		});
//		
	 }
		
	
	
	 @Override
		public void onResume(){
		    super.onResume();
		  
		}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
}
