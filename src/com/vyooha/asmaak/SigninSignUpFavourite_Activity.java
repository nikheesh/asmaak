package com.vyooha.asmaak;

import com.google.android.gms.internal.ao;
import com.parse.ParseUser;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

public class SigninSignUpFavourite_Activity extends Activity{
	Button btn_signin,btn_signup,btn_favourites;
	TextView txt_cart,txt_appinfo,txt_faq,txt_share,txt_delivery,txt_contact,txt_signout;
	ImageButton imgbtn_cart,imgbtn_back,imgbtn_appinfo,imgbtn_faq,imgbtn_share,imgbtn_delivery,imgbtn_contact,imgbtn_signout;
	
	View line8;
	
	RelativeLayout rl_favourite;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		
		super.onCreate(savedInstanceState);

		requestWindowFeature(Window.FEATURE_NO_TITLE);
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);
		
		setContentView(R.layout.signinsignupfavourite_activity);
		
		btn_favourites=(Button)findViewById(R.id.btn_signinsignupfav_favourites);
		rl_favourite=(RelativeLayout)findViewById(R.id.rl_signinsignupfav_favourites);
		
		btn_signin=(Button)findViewById(R.id.btn_signinsignupfav_signin);
		btn_signup=(Button)findViewById(R.id.btn_signinsignupfav_signup);
		
		txt_cart=(TextView)findViewById(R.id.txt_signinsignupfav_cart);
		imgbtn_cart=(ImageButton)findViewById(R.id.imgbtn_signinsignupfav_cart);
		
		txt_appinfo=(TextView)findViewById(R.id.txt_signinsignupfav_appinfo);
		imgbtn_appinfo=(ImageButton)findViewById(R.id.imgbtn_signinsignupfav_appinfo);
		
		txt_faq=(TextView)findViewById(R.id.txt_signinsignupfav_faq);
		imgbtn_faq=(ImageButton)findViewById(R.id.imgbtn_signinsignupfav_faq);
		
		txt_share=(TextView)findViewById(R.id.txt_signinsignupfav_share);
		imgbtn_share=(ImageButton)findViewById(R.id.imgbtn_signinsignupfav_share);
		
		txt_contact=(TextView)findViewById(R.id.txt_signinsignupfav_contactus);
		imgbtn_contact=(ImageButton)findViewById(R.id.imgbtn_signinsignupfav_contactus);
		
		txt_delivery=(TextView)findViewById(R.id.txt_signinsignupfav_delivery);
		imgbtn_delivery=(ImageButton)findViewById(R.id.imgbtn_signinsignupfav_delivery);
		
		txt_signout=(TextView)findViewById(R.id.txt_signinsignupfav_signout);
		imgbtn_signout=(ImageButton)findViewById(R.id.imgbtn_signinsignupfav_signout);
		line8=(View)findViewById(R.id.line8);
		
		imgbtn_back=(ImageButton)findViewById(R.id.imgbtn_common_back);
		
		ParseUser user=ParseUser.getCurrentUser();
		if(user==null){
			rl_favourite.setVisibility(View.GONE);
			txt_signout.setVisibility(View.GONE);
			imgbtn_signout.setVisibility(View.GONE);
			line8.setVisibility(View.GONE);
		}
		else{
			rl_favourite.setVisibility(View.VISIBLE);
			btn_signin.setVisibility(View.GONE);
			btn_signup.setVisibility(View.GONE);
			txt_signout.setVisibility(View.VISIBLE);
			imgbtn_signout.setVisibility(View.VISIBLE);
			line8.setVisibility(View.VISIBLE);
		}
		
		
		txt_cart.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
			
				finish();
				Intent i=new Intent(getApplicationContext(), CartListCustomer_Activity.class);
				startActivity(i);
				overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
			}
		});
		imgbtn_cart.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
			
				finish();
				Intent i=new Intent(getApplicationContext(), CartListCustomer_Activity.class);
				startActivity(i);
				overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
				
			}
		});
		
		txt_appinfo.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				finish();
				Intent i=new Intent(getApplicationContext(), About_Activity.class);
				startActivity(i);
				overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
					
			}
		});
		
		imgbtn_appinfo.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				finish();
				Intent i=new Intent(getApplicationContext(), About_Activity.class);
				startActivity(i);
				overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
					
			}
		});
		
		txt_share.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				finish();
				Intent i=new Intent(getApplicationContext(), Share_Activity.class);
				startActivity(i);
				overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
					
			}
		});
		
		imgbtn_share.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				finish();
				Intent i=new Intent(getApplicationContext(), Share_Activity.class);
				startActivity(i);
				overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
					
			}
		});
		
		txt_faq.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				finish();
				Intent i=new Intent(getApplicationContext(), FAQ_Activity.class);
				startActivity(i);
				overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
					
			}
		});
		
		imgbtn_faq.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				finish();
				Intent i=new Intent(getApplicationContext(), FAQ_Activity.class);
				startActivity(i);
				overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
					
			}
		});
		
		
		txt_contact.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				finish();
				Intent i=new Intent(getApplicationContext(), Contacts_Activity.class);
				startActivity(i);
				overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
					
			}
		});
		
		imgbtn_contact.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				finish();
				Intent i=new Intent(getApplicationContext(), Contacts_Activity.class);
				startActivity(i);
				overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
					
			}
		});
		
		
		txt_delivery.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				finish();
				Intent i=new Intent(getApplicationContext(), Delivery_Activity.class);
				startActivity(i);
				overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
					
			}
		});
		
		imgbtn_delivery.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				finish();
				Intent i=new Intent(getApplicationContext(), Delivery_Activity.class);
				startActivity(i);
				overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
					
			}
		});
		
		txt_signout.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				
				ParseUser.logOut();
				Toast.makeText(getApplicationContext(), "Successfully Logged out", Toast.LENGTH_LONG).show();
				finish();
				overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
				
			}
		});
		
		imgbtn_signout.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				
				ParseUser.logOut();
				Toast.makeText(getApplicationContext(), "Successfully Logged out", Toast.LENGTH_LONG).show();
				finish();
				overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
				
			}
		});
		
		
		
		
		
		imgbtn_back.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
			finish();
			overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
			}
		});
		
		
		
		
		btn_signin.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				
				Intent i=new Intent(getApplicationContext(), LoginAndSignup_Activity.class);
				//i.putExtra("totalprice", getIntent().getFloatExtra("totalprice", 0));
				i.putExtra("time", "signin");
				startActivity(i);		
				
			}
		});
		
		btn_signup.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Intent i=new Intent(getApplicationContext(), LoginAndSignup_Activity.class);
				//i.putExtra("totalprice", getIntent().getFloatExtra("totalprice", 0));
				i.putExtra("time", "signup");
				startActivity(i);	
			}
		});
		
		btn_favourites.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
			
				Intent i=new Intent(getApplicationContext(), ItemListFavouriteCustomer_Activity.class);
				//i.putExtra("category", "favourites");
				startActivity(i);
				overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
			}
		});
		
		
	}
	
	@Override
	public void onBackPressed() {
		finish();
		overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
	}

}
