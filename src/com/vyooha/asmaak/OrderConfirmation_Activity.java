package com.vyooha.asmaak;

import java.io.ObjectOutputStream.PutField;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.json.JSONException;
import org.json.JSONObject;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.parse.FunctionCallback;
import com.parse.ParseCloud;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseUser;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.View.OnClickListener;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RadioGroup;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

public class OrderConfirmation_Activity	extends Activity {
	
	ArrayAdapter<CartItemClass> contactAdapter;
	ListView list;
	ParseUser user;
	MyDB db;
	float totalprice=0,alltotal=0;
	ImageButton imgbtn_option,imgbtn_cart;
	//EditText edit_search;
	TextView txt_head;
	
	ProgressWheel progressdialogue;
	ScrollView scrollview;
	//TextView txt_address;// edit_email,edit_phone,edit_address;
	Button order_now;
	final DisplayImageOptions options = new DisplayImageOptions.Builder()
	.cacheInMemory(true).cacheOnDisc(true)
	.resetViewBeforeLoading(true)
	// .showImageForEmptyUri(fallback)
	// .showImageOnFail(fallback)
	// .showImageOnLoading(fallback)
	.build();
	private static String[] mMonth = new String[] { "Jan", "Feb", "Mar", "Apr", "May",
		"Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec" };

	TextView txt_arrivedate,txt_arrivetime,txt_phone,txt_address,txt_comment,txt_subtotal,txt_tax,txt_delivery,txt_total;
	ImageButton imgbtn_arrivetime,imgbtn_phone,imgbtn_address,imgbtn_comment;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState); 
		
		user=ParseUser.getCurrentUser();
		db=new MyDB(getApplicationContext());
		
		contactAdapter = new ArrayAdapter<CartItemClass>(this, 0) {
			@Override
			public View getView(int position, View convertView, ViewGroup parent) {
				if (convertView == null) {
					convertView = getLayoutInflater().inflate(
							R.layout.cartitem, null);
				}
 
				final CartItemClass it=getItem(position);

				ImageView imageView = (ImageView) convertView
							.findViewById(R.id.img_cartitem_itemimg);
					
					TextView txt_name = (TextView) convertView
							.findViewById(R.id.txt_cartitem_name);
						TextView txt_processingtype=(TextView)convertView.findViewById(R.id.txt_cartitem_cleaning_charge);
						TextView txt_cartitem_subtotal=(TextView)convertView.findViewById(R.id.txt_cartitem_subtotal);
						TextView txt_cartitem_qty=(TextView)convertView.findViewById(R.id.txt_cartitem_quantity);
						final EditText edit_qty=(EditText)convertView.findViewById(R.id.edit_cartitem_quantity);
						ImageLoader imageLoader = ImageLoader.getInstance();
						
						Button btn_add=(Button)convertView.findViewById(R.id.btn_cartitem_add);
						Button btn_sub=(Button)convertView.findViewById(R.id.btn_cartitem_sub);
						
						txt_name.setText(it.name);
						txt_processingtype.setText(it.processings);
						edit_qty.setText(it.quantity+"");
						float subtotal=0;
						if(it.iscleanRequiered){
							subtotal=it.quantity*(it.cleaning_charge+it.unit_price);
								}
						else
							subtotal=it.quantity*it.unit_price;
						
						txt_cartitem_subtotal.setText("QR "+subtotal);
						
						txt_cartitem_qty.setText("/"+it.quantity+" "+it.uom);
					
					
						btn_add.setOnClickListener(new OnClickListener() {
			
							@Override
							public void onClick(View v) {
								it.quantity++;
								edit_qty.setText(Integer.toString(it.quantity));
								
								contactAdapter.notifyDataSetChanged();
								if(user==null){
									db.updateCartQuantity(it.obid, it.quantity);
								}
								else{
									
									
									it.handler.removeCallbacksAndMessages(null);
									it.handler.postDelayed(new Runnable() {
									    @Override
									    public void run() {
									       Log.e("handler  working  for ",it.name+",qty="+it.quantity);
									    
									       Map<String, Object> inputparm = new HashMap<String, Object>();
											
											inputparm.put("quantity", it.quantity);
											inputparm.put("fish_id", it.fishidsignin);
											
											inputparm.put("isCleanRequired", it.iscleanRequiered);
											ArrayList<String> processings=new ArrayList<String>();
											processings.add(it.processings);
											inputparm.put("processings", processings);
								
												try {
													ParseCloud.callFunction("addToCart", inputparm);
												} catch (ParseException e) {
													// TODO Auto-generated catch block
													e.printStackTrace();
												}	       
									       
									       
									       
									       
									       
									       
									    }
									}, 5000);
								}
								
								
							}
						});
						btn_sub.setOnClickListener(new OnClickListener() {
			
							@Override
							public void onClick(View v) {
								if(it.quantity>1)
									it.quantity--;
								edit_qty.setText(Integer.toString(it.quantity));
								
								contactAdapter.notifyDataSetChanged();
								if(user==null){
									db.updateCartQuantity(it.obid, it.quantity);
								}
								else{
									
									
									it.handler.removeCallbacksAndMessages(null);
									it.handler.postDelayed(new Runnable() {
									    @Override
									    public void run() {
									       Log.e("handler  working  for ",it.name+",qty="+it.quantity);
									    
									       Map<String, Object> inputparm = new HashMap<String, Object>();
											
											inputparm.put("quantity", it.quantity);
											inputparm.put("fish_id", it.fishidsignin);
											
											inputparm.put("isCleanRequired", it.iscleanRequiered);
											ArrayList<String> processings=new ArrayList<String>();
											processings.add(it.processings);
											inputparm.put("processings", processings);
								
												try {
													ParseCloud.callFunction("addToCart", inputparm);
												} catch (ParseException e) {
													// TODO Auto-generated catch block
													e.printStackTrace();
												}	       
									       
									       
									       
									       
									       
									       
									    }
									}, 5000);
								}
								
								
								
								}
							});
					
					if (it.imgurl != null) {
						imageLoader.displayImage(it.imgurl, imageView);

					} else {
						imageView.setImageResource(R.drawable.ic_launcher);
					}
					
				
				return convertView;
			}
		};

		
		
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);

		
		
		setContentView(R.layout.activity_orderconfirm);
		txt_address=(TextView)findViewById(R.id.txt_orderconfirm_address);
		//edit_email=(EditText)findViewById(R.id.edit_orderconfirm_email);
		//edit_phone=(EditText)findViewById(R.id.edit_orderconfirm_phone);
		order_now=(Button)findViewById(R.id.btn_orderconfirm_placeorder);
		txt_address=(TextView)findViewById(R.id.txt_orderconfirm_address);
		txt_arrivedate=(TextView)findViewById(R.id.txt_orderconfirm_arrivedate);
		txt_arrivetime=(TextView)findViewById(R.id.txt_orderconfirm_arrivetime);
		txt_comment=(TextView)findViewById(R.id.txt_orderconfirm_comment);
		txt_delivery=(TextView)findViewById(R.id.txt_orderconfirm_delivery);
		txt_phone=(TextView)findViewById(R.id.txt_orderconfirm_contactno);
		txt_subtotal=(TextView)findViewById(R.id.txt_orderconfirm_subtotal);
		txt_tax=(TextView)findViewById(R.id.txt_orderconfirm_tax);
		txt_total=(TextView)findViewById(R.id.txt_orderconfirm_total);
		
		imgbtn_address=(ImageButton)findViewById(R.id.imgbtn_orderconfirm_address);
		imgbtn_arrivetime=(ImageButton)findViewById(R.id.imgbtn_orderconfirm_arrivetime);
		imgbtn_comment=(ImageButton)findViewById(R.id.imgbtn_orderconfirm_comment);
		imgbtn_phone=(ImageButton)findViewById(R.id.imgbtn_orderconfirm_contactno);
		
		
//		edit_search=(EditText)findViewById(R.id.edit_common_search);
//		edit_search.setVisibility(View.GONE);
		txt_head=(TextView)findViewById(R.id.txt_common_head);
		scrollview=(ScrollView)findViewById(R.id.scrollview_orderconfirm_scrollview);
		progressdialogue=(ProgressWheel)findViewById(R.id.progress_orderconfirm_progress);
		
		progressdialogue.setVisibility(View.VISIBLE);
		progressdialogue.spin();
		scrollview.setVisibility(View.GONE);
		
		
		
		imgbtn_option=(ImageButton)findViewById(R.id.imgbtn_common_option);
		imgbtn_option.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				finish();
			Intent i=new Intent(getApplicationContext(), SigninSignUpFavourite_Activity.class);
			startActivity(i);
			}
		});
		
//		
	imgbtn_cart=(ImageButton)findViewById(R.id.imgbtn_common_search);
//		imgbtn_cart.setOnClickListener(new OnClickListener() {
//			
//			@Override
//			public void onClick(View v) {
//			Intent i=new Intent(getApplicationContext(),ItemListCustomer_Activity.class);
//			i.putExtra("category", "all");
//			startActivity(i);
//			}
//		});
//		
//		
imgbtn_cart.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
			
				Intent intent=new Intent(getApplicationContext(), CartListCustomer_Activity.class);
				startActivity(intent);
				overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
//				edit_search.setVisibility(View.VISIBLE);
//				txt_head.setVisibility(View.GONE);
			}
		});
		
//		edit_search.setOnEditorActionListener(new TextView.OnEditorActionListener() {
//		    @Override
//		    public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
//		        if (actionId == EditorInfo.IME_ACTION_SEARCH) {
//		        	
//		        	String tag=edit_search.getText().toString();
//		           
//		        	
//		        	
//		        	InputMethodManager imm = (InputMethodManager) getSystemService(
//		    				Context.INPUT_METHOD_SERVICE);
//		    		imm.hideSoftInputFromWindow(edit_search.getWindowToken(), 0);
//		    		
//		    		Intent i=new Intent(getApplicationContext(), ItemListCustomer_Activity.class);
//		    		i.putExtra("category", tag);
//		    		startActivity(i);
//		            edit_search.setVisibility(View.GONE);
//		        	txt_head.setVisibility(View.VISIBLE);
//		    		
//		            return true;		            
//		        }
//		        return false;
//		    }
//		}); 
//		
		
		
		
		list = (ListView)findViewById(R.id.listview_orderconfirm_list);
		list.setAdapter(contactAdapter);
		
		if(user==null){
			Cursor c1=db.selectCart();
			
			progressdialogue.setVisibility(View.GONE);
			progressdialogue.stopSpinning();
			scrollview.setVisibility(View.VISIBLE);
			
			for(int i=0;i<c1.getCount();i++){
				c1.moveToPosition(i);
				CartItemClass cartitclass=new CartItemClass();
				cartitclass.name=c1.getString(1);
				cartitclass.obid=c1.getString(0);
				cartitclass.iscleanRequiered=Boolean.parseBoolean(c1.getString(2));
				cartitclass.processings=c1.getString(3);
				cartitclass.quantity=c1.getInt(4);
				cartitclass.unit_price=c1.getFloat(5);
				cartitclass.cleaning_charge=c1.getFloat(6);
				cartitclass.imgurl=c1.getString(7);
				cartitclass.uom=c1.getString(8);
				cartitclass.fishidsignin=cartitclass.obid;
				
				float subtotal=0;
				if(cartitclass.iscleanRequiered){
					subtotal=cartitclass.quantity*(cartitclass.cleaning_charge+cartitclass.unit_price);
					}
				else
					subtotal=cartitclass.quantity*cartitclass.unit_price;
			
				totalprice+=subtotal;
				// {"fishid","fishname","isclean","processings","quantity","price","cleaningcharge","imgurl","uom"
			contactAdapter.add(cartitclass);
			
			}
			Utilities_Functions.setListViewHeightBasedOnChildren(list);
			
		//	txt_totalprice.setText("QR "+totalprice);
			txt_subtotal.setText("QR "+totalprice);
			 alltotal=totalprice+4+7;
			txt_total.setText("QR "+alltotal);
		
			
		}
		else{
			
		try{
			
			txt_address.setText(user.getString("address"));
			Log.e("Address Retrieved", user.getString("address"));
		}
			catch(Exception e){
				txt_address.setText("Add delivery address");
				Log.e("error on address retrieved", e.toString());
			}
	
	try{
			
			txt_phone.setText(user.getString("phone"));
		}
			catch(Exception e){
				
			}
	
		
		HashMap<String, String> input_param = new HashMap<String, String>();
		
		ParseCloud.callFunctionInBackground("viewCart", input_param,
				new FunctionCallback<List<ParseObject>>() {

					@Override
					public void done(List<ParseObject> result,
							ParseException e) {
						// TODO Auto-generated method stub
						progressdialogue.setVisibility(View.GONE);
						progressdialogue.stopSpinning();
						scrollview.setVisibility(View.VISIBLE);
						
						if (e == null) {
							for(int i=0;i<result.size();i++){
								ParseObject item=result.get(i);
								ParseObject fish=item.getParseObject("fish_obj");
								CartItemClass cartitclass=new CartItemClass();
								cartitclass.cleaning_charge=Float.parseFloat(fish.getNumber("processing_charge").toString());
								
								try{
								cartitclass.imgurl=fish.getParseFile("fish_img").getUrl();
								}
								catch(Exception e2){
									
								}
								
								cartitclass.iscleanRequiered=item.getBoolean("isCleanRequired");
								cartitclass.name=fish.getString("fish_name");
								cartitclass.obid=item.getObjectId();
								cartitclass.fishidsignin=fish.getObjectId();
								try {
									cartitclass.processings=item.getJSONArray("processings").getString(0);
								} catch (JSONException e1) {
									// TODO Auto-generated catch block
									e1.printStackTrace();
								}
								cartitclass.quantity=item.getInt("quantity");
								cartitclass.unit_price=(float) fish.getDouble("unit_price");
								cartitclass.uom=fish.getString("uom");
								
								float subtotal=0;
								if(cartitclass.iscleanRequiered){
									subtotal=cartitclass.quantity*(cartitclass.cleaning_charge+cartitclass.unit_price);
								}
								else
									subtotal=cartitclass.quantity*cartitclass.unit_price;
							
								totalprice+=subtotal;
								
								
								contactAdapter.add(cartitclass);
							
							}
							Utilities_Functions.setListViewHeightBasedOnChildren(list);
							
							txt_subtotal.setText("QR "+totalprice);
						 alltotal=totalprice+4+7;
							txt_total.setText("QR "+alltotal);
						}
						else {
							Log.e("error while loading", e.toString());
						}
						
					
					
					}
					
		
		});
		
		}

		
		
		//
		
		Calendar today = Calendar.getInstance();
		String todaystr=mMonth[today.get(Calendar.MONTH)]+" "+today.get(Calendar.DAY_OF_MONTH)+" "+today.get(Calendar.YEAR);
		txt_arrivedate.setText(todaystr);
		
		
		
		
		order_now.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				
				
				String addr=txt_address.getText().toString();
				String phone=txt_phone.getText().toString();
				
				if(!phone.equals("No contact selected")&&!phone.equals("")&&!addr.equals("Add delivery address")&&!addr.equals(""))
				{
				JSONObject json=new JSONObject();
				try{
				json.put("address", addr);
				json.put("phone", phone);
				json.put("deliveryslot", txt_arrivetime.getText().toString());
				json.put("comments", txt_comment.getText().toString());
				}
				catch(Exception e){
					
				}
					
				AsmaakApplication appState = ((AsmaakApplication) getApplicationContext());
				appState.editor.putString("orderhead", json.toString());
				appState.editor.commit();
				
				
				
					if(user==null){finish();
						Intent i=new Intent(getApplicationContext(), Not_SignedIn_Activity.class);
						i.putExtra("totalprice", alltotal);
						startActivity(i);
						
					}
					else{
						finish();
						Intent i=new Intent(getApplicationContext(), Confirm_Activity.class);
						i.putExtra("totalprice", alltotal);
						startActivity(i);
					}
					
				}
				
				else{
					
					if(phone.equals("No contact selected")||phone.equals("")){
						Toast.makeText(getApplicationContext(), "Please add your contact ", Toast.LENGTH_LONG).show();
					}
					else{
						Toast.makeText(getApplicationContext(), "Please add your address ", Toast.LENGTH_LONG).show();
						
					}
				}
				
				
				
			}
		});
		
		imgbtn_arrivetime.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
			alertlistarrivetime();	
			}
		});
		
		
		imgbtn_address.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				alertaddress();
			}
		});
		
		imgbtn_comment.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
			alertcomment();	
			}
		});
		imgbtn_phone.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
			alertphone();	
			}
		});
		
	}
	
	
	public void alertlistarrivetime(){
	
	AlertDialog.Builder builderSingle = new AlertDialog.Builder(
            OrderConfirmation_Activity.this);
    builderSingle.setIcon(R.drawable.ic_launcher);
    builderSingle.setTitle("Select Payment Type:-");
    final ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(
    OrderConfirmation_Activity.this,
            android.R.layout.select_dialog_singlechoice);
    arrayAdapter.add("9.00-12.00am");
    arrayAdapter.add("12.00-3.00pm");
    arrayAdapter.add("3.00-6.00pm");
    builderSingle.setNegativeButton("cancel",
            new DialogInterface.OnClickListener() {

                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });

    builderSingle.setAdapter(arrayAdapter,
            new DialogInterface.OnClickListener() {

                @Override
                public void onClick(DialogInterface dialog, int which) {
                    String strName = arrayAdapter.getItem(which);
                  txt_arrivetime.setText(strName);
                }
            });
    builderSingle.show();


	
}
	
	public void alertaddress(){
		
		final Dialog dialog = new Dialog(OrderConfirmation_Activity.this);
		dialog.setContentView(R.layout.dialogue_address);
		dialog.setTitle("Add Address");
		final	EditText edit_name=(EditText)dialog.findViewById(R.id.edit_orderconfirmation_name);
		
	final	EditText edit_addr1=(EditText)dialog.findViewById(R.id.edit_orderconfirmation_address1);
	final	EditText edit_addr2=(EditText)dialog.findViewById(R.id.edit_orderconfirmation_address2);
	final	EditText edit_city=(EditText)dialog.findViewById(R.id.edit_orderconfirmation_city);
	final	EditText edit_state=(EditText)dialog.findViewById(R.id.edit_orderconfirmation_state);
	final	EditText edit_country=(EditText)dialog.findViewById(R.id.edit_orderconfirmation_country);
	final	EditText edit_zip=(EditText)dialog.findViewById(R.id.edit_orderconfirmation_zip);
		edit_country.setText("Quatar");
		edit_country.setEnabled(false);
		
		Button dialogButton = (Button) dialog.findViewById(R.id.btn_orderconfirmation_addressok);
		// if button is clicked, close the custom dialog
		dialogButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				
				txt_address.setText(edit_name.getText().toString()+"\n"+edit_addr1.getText().toString()+"\n"+edit_addr2.getText().toString()+"\n"+edit_city.getText()+"\n"+edit_state.getText().toString()+"\n"+edit_country.getText().toString()+"\nzip code: "+edit_zip.getText().toString());
				
				
				
				
				dialog.dismiss();
			}
		});

		dialog.show();

		
	}	
	
	public void alertcomment(){
		
		final Dialog dialog = new Dialog(OrderConfirmation_Activity.this);
		dialog.setContentView(R.layout.dialogue_comment);
		dialog.setTitle("Add Comment");

	final	EditText edit_comment=(EditText)dialog.findViewById(R.id.edit_orderconfirmation_comment);
		Button dialogButton = (Button) dialog.findViewById(R.id.btn_orderconfirmation_commentok);
		// if button is clicked, close the custom dialog
		dialogButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				
				txt_comment.setText(edit_comment.getText().toString());
				
				
				
				dialog.dismiss();
			}
		});

		dialog.show();

		
	}	
	
	
public void alertphone(){
		
		final Dialog dialog = new Dialog(OrderConfirmation_Activity.this);
		dialog.setContentView(R.layout.dialogue_phone);
		dialog.setTitle("Add Contact");
		
	final	EditText edit_contact=(EditText)dialog.findViewById(R.id.edit_orderconfirmation_contact);
		Button dialogButton = (Button) dialog.findViewById(R.id.btn_orderconfirmation_contactok);
		// if button is clicked, close the custom dialog
		dialogButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				
				txt_phone.setText(edit_contact.getText().toString());
				
				
				
				dialog.dismiss();
			}
		});

		dialog.show();

		
	}	
	
	
}
